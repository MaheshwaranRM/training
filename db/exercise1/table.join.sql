SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,subjects.subject_name
	  ,subjects.subject_credit
  FROM exercise1.students INNER JOIN exercise1.subjects ON students.subject_id = subjects.subject_id;
  
SELECT students.name
      ,students.surname
	  ,subjects.subject_name 
  FROM exercise1.subjects LEFT JOIN exercise1.students ON subjects.subject_id = students.subject_id;
  
SELECT students.student_id
      ,subjects.subject_id
	  ,subjects.subject_name
  FROM exercise1.students RIGHT JOIN exercise1.subjects ON students.subject_id = subjects.subject_id;
  
SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,subjects.subject_name
	  ,subjects.subject_credit
	  ,students.marks 
  FROM exercise1.students
      ,exercise1.subjects;
