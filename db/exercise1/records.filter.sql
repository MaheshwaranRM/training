SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,students.marks
  FROM exercise1.students
 WHERE students.name = 'Rahul' AND students.surname = 'N'
 
SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,students.marks
  FROM exercise1.students
 WHERE students.subject_id = '101' OR students.subject_id = '102'
 
SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,students.marks
  FROM exercise1.students
 WHERE NOT students.subject_id='105'
 
SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,students.marks
  FROM exercise1.students
 WHERE students.name LIKE 'M%'
 
SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
	  ,students.marks
  FROM exercise1.students
 WHERE students.name IN ('Rahul','Praveen','Mervin','Lokesh','Prasath')
 
SELECT students.name
  FROM exercise1.students
 WHERE student_id = ANY (SELECT student_id FROM exercise1.subjects WHERE subject_name = 'Tamil')
 
 
 