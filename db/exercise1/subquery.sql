SELECT students.student_id
      ,students.name
	  ,students.surname
	  ,students.subject_id
  FROM exercise1.students
 WHERE subject_id IN (SELECT subject_id FROM exercise1.subjects WHERE subjects.subject_name = 'Maths')