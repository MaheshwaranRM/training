    ALTER TABLE `exercise1`.`students` 
  CHANGE COLUMN `student_id` `student_id` INT(11) NOT NULL ;

    ALTER TABLE `exercise1`.`students` 
  CHANGE COLUMN `student_id` `student_id` INT(11) NOT NULL DEFAULT 0 ;
	
	ALTER TABLE exercise1.students
      ADD CHECK (student_id <= 50);
	
	ALTER TABLE `exercise1`.`students` 
ADD PRIMARY KEY (`student_id`);

    ALTER TABLE `exercise1`.`students` 
      ADD INDEX `subject_id_idx` (`subject_id` ASC) VISIBLE;
    ALTER TABLE `exercise1`.`students` 
 ADD CONSTRAINT `subject_id`
    FOREIGN KEY (`subject_id`)
     REFERENCES `exercise1`.`subjects` (`subject_id`)
ON DELETE RESTRICT
ON UPDATE CASCADE;

   CREATE INDEX index_student_id
             ON exercise1.students ( student_id );

    

