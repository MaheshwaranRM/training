  ALTER TABLE `exercise1`.`students` 
   ADD COLUMN `rollno` INT(11)
   NULL AFTER `name`;
 
  ALTER TABLE exercise1.students 
MODIFY COLUMN rollno INT(11) NULL;

  ALTER TABLE exercise1.students 
RENAME COLUMN rollno TO student_id ;

  ALTER TABLE exercise1.students 
  DROP COLUMN rollno ;
