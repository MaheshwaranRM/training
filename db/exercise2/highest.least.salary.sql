SELECT MAX(employee.annual_salary) 
    AS highest_salary
  FROM database.employee

SELECT MIN(employee.annual_salary) 
    AS least_salary
  FROM database.employee
