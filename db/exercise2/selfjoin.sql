SELECT emp1.first_name AS first_name1
                        , emp2.first_name AS first_name2
						                   , emp1.department_number
  FROM database.employee emp1
      ,database.employee emp2
WHERE  emp1.emp_id<> emp2.emp_id
  AND emp1.department_number = emp2.department_number 
