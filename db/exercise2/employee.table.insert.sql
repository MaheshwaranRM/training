INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)

     VALUES 
	  ('001'
	 , 'Mahesh'
	 , 'R'
	 , '2001/02/09'
	 , '2016/09/09'
	 , '2000000'
	 , '1');
	 
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)

     VALUES 
	   ('002'
	  , 'Praveen'
	  , 'M'
	  , '2000/06/06'
	  , '2015/12/17'
	  , '3000000'
	  , '1');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`) 
     VALUES 
	    ('003'
	   , 'Rahul'
	   , 'B'
	   , '2001/09/22'
	   , '2016/02/22'
	   , '40000000'
	   , '1');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
	  ('004'
	 , 'Mukilan'
	 , 'R'
	 , '2001/09/28'
	 , '2013/11/18'
	 , '12000000'
	 , '1');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`) 
     VALUES 
	   ('005'
	  , 'Kishore'
	  , 'S'
	  , '2000/01/18'
	  , '2016/10/01'
	  , '11000000'
	  , '1');
INSERT INTO `database`.`employee`
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('006'
		 ,'Naveen'
		 , 'N'
		 , '1999/08/22'
		 , '2015/12/28'
		 , '3000000'
		 , '2');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
      	  ('007'
		 , 'Pradeep', 'N'
		 , '2000/08/08'
		 , '2017/11/12'
		 , '5000000'
		 , '2');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	   ('008'
		  , 'Nishanth'
		  , 'V'
		  , '1999/09/01'
		  , '2016/03/20'
		  , '50000000'
		  , '2');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
	        ('009'
		   , 'Prasath'
		   , 'R'
		   , '1999/11/09'
		   , '2015/04/27'
		   , '6000000'
		   , '2');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
        	('010'
			, 'Manoj'
			, 'D'
			, '2000/12/24'
			, '2015/12/17'
			, '2000000'
			, '2');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`) 
     VALUES
    	 ('011'
		 , 'Nandha Kumar'
		 , 'F'
		 , '2000/04/04'
		 , '2016/02/22'
		 , '11000000'
		 , '3');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('012'
		 , 'Raj'
		 , 'K'
		 , '2000/08/08'
		 , '2013/11/18'
		 , '1500000'
		 , '3');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`) 
     VALUES
    	 ('013'
		 , 'Karthick'
		 , 'J'
		 , '2000/01/18'
		 , '2015/12/28'
		 , '3000000'
		 , '3');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES 
	    ('014'
		, 'Senthil'
		, 'G'
		, '2000/06/06'
		, '2016/09/09'
		, '5000000'
		, '3');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('015'
		 , 'Arun'
		 , 'S'
		 , '1999/09/01'
		 , '2016/02/22'
		 , '2000000'
		 , '3');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES 
	     ('016'
		 , 'Ajay'
		 , 'A'
		 , '2001/09/28'
		 , '2015/12/17'
		 , '8000000'
		 , '4');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES 
	     ('017'
		 , 'Aravindh'
		 , 'R', '2000/08/08'
		 , '2013/11/18'
		 , '10000000'
		 , '4');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`) 
     VALUES
    	 ('018'
		 , 'Bala'
		 , 'E'
		 , '200/06/06'
		 , '2015/12/28'
		 , '9000000'
		 , '4');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('019'
		 , 'Vishwa'
		 , 'M'
		 , '1999/09/01'
		 , '2016/09/09'
		 , '3000000'
		 , '4');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('020'
		 , 'Gokul'
		 , 'N'
		 , '2001/09/28'
		 , '2016/02/22'
		 , '3000000'
		 , '4');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES 
	     ('021'
		 , 'Rajesh'
		 , 'B'
		 , '2000/08/08'
		 , '2013/11/18'
		 , '45000000'
		 , '5');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
	     ('022'
		 , 'Mervin'
		 , 'L'
		 , '200/06/06'
		 , '2015/12/17'
		 , '2700000'
		 , '5');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('023'
		 , 'Kiran'
		 , 'K'
		 , '1999/09/01'
		 , '2016/02/22'
		 , '1600000'
		 , '5');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('024'
		 , 'Nandha'
		 , 'D'
		 , '2000/08/08'
		 , '2013/11/18'
		 , '4000000'
		 , '5');
INSERT INTO `database`.`employee` 
(`emp_id`
, `first_name`
, `surname`
, `dob`
, `date_of_joining`
, `annual_salary`
, `department_number`)
     VALUES
    	 ('025'
		 , 'Varun'
		 , 'S'
		 , '2001/09/28'
		 , '2016/09/09'
		 , '8000000'
		 , '5');
INSERT INTO `database`.`employee` (`department_number`) 
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`) 
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`) 
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`) 
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`)
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`)
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`)
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`)
     VALUES ('');
INSERT INTO `database`.`employee` (`department_number`)
     VALUES ('');
