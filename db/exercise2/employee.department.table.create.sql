
CREATE TABLE `database`.`employee`
 (
  `emp_id` INT NOT NULL
  ,`first_name` VARCHAR(45) NOT NULL
  ,`surname` VARCHAR(45) NULL
  ,`dob` DATE NOT NULL
  ,`date_of_joining` DATE NOT NULL
  ,`annual_salary` INT NOT NULL
 );
  ALTER TABLE `database`.`employee` 
   ADD COLUMN `department_number` VARCHAR(45) NOT NULL AFTER `annual_salary`;
  ALTER TABLE `database`.`employee` 
CHANGE COLUMN `emp_id` `emp_id` INT NOT NULL AUTO_INCREMENT ;

CREATE TABLE `database`.`department`
 (
   `department_number` INT NOT NULL
  ,`department_name` VARCHAR(45) NOT NULL
  ,PRIMARY KEY (`department_number`)
);

   ALTER TABLE `database`.`employee` 
   ALTER INDEX `department_number_idx` VISIBLE;
   ALTER TABLE `database`.`employee` 
ADD CONSTRAINT `department_number`
   FOREIGN KEY (`department_number`)
    REFERENCES `database`.`department` (`department_number`)
ON DELETE RESTRICT
ON UPDATE CASCADE;