 
SELECT employee.first_name
      ,employee.surname
	  ,employee.dob AS birthday
  FROM employee 
 WHERE DATE_FORMAT(dob,'%d-%m') = DATE_FORMAT(SYSDATE(),'%d-%m');