CREATE SCHEMA education;

 CREATE TABLE education.university(
             univ_code CHAR(4) NOT NULL
            ,university_name VARCHAR(100) NOT NULL
            ,PRIMARY KEY (univ_code));
			
 CREATE TABLE `education`.`designation`(
            `id` INT NOT NULL
           ,`name` VARCHAR(30) NOT NULL,
            `rank` CHAR(1) NOT NULL
			,PRIMARY KEY (`id`));
			
 CREATE TABLE education.college (
              id INT AUTO_INCREMENT
             ,code CHAR(4) NOT NULL
             ,name VARCHAR(100) NOT NULL
             ,univ_code CHAR(4) NULL
             ,city VARCHAR(50) NOT NULL
             ,state VARCHAR(50) NOT NULL
             ,year_opened YEAR NOT NULL
             ,PRIMARY KEY (id)
             ,INDEX univ_idx (univ_code ASC) VISIBLE
             ,CONSTRAINT univ
                 FOREIGN KEY (univ_code)
                  REFERENCES education.university(univ_code)
                   ON DELETE NO ACTION
                   ON UPDATE NO ACTION);
				   
CREATE TABLE `education`.`department` (
            `dept_code` INT NOT NULL
		   ,`dept_name` VARCHAR(50) NULL
		   ,`univ_code` CHAR(4) NULL
		   ,PRIMARY KEY (`dept_code`)
		   ,INDEX `univ_code_idx` (`univ_code` ASC) VISIBLE
		   ,CONSTRAINT `univ_code`
                FOREIGN KEY (`univ_code`)
                 REFERENCES `education`.`university` (`univ_code`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION);
				  
CREATE TABLE `education`.`college_department` (
            `cdept_id` INT NOT NULL
			,`udept_code` CHAR(4) NULL
			,`college_id` INT NULL
			,PRIMARY KEY (`cdept_id`)
			,INDEX `college_id_idx` (`college_id` ASC) VISIBLE
			,CONSTRAINT `udept_code`
                 FOREIGN KEY (`udept_code`)
                  REFERENCES `education`.`department` (`univ_code`)
                   ON DELETE NO ACTION
                   ON UPDATE NO ACTION
				   ,CONSTRAINT `college_id`
                        FOREIGN KEY (`college_id`)
                         REFERENCES `education`.`college` (`id`)
                          ON DELETE NO ACTION
                          ON UPDATE NO ACTION);
						  
CREATE TABLE `education`.`student` (
            `id` INT NOT NULL
		   ,`roll_number` CHAR(8) NOT NULL
		   ,`name` CHAR(8) NOT NULL
		   ,`dob` DATE NOT NULL
		   ,`gender` CHAR(1) NOT NULL
		   ,`email` VARCHAR(50) NOT NULL
		   ,`phone` BIGINT NOT NULL
		   ,`address` VARCHAR(200) NOT NULL
		   ,`academic_year` YEAR NOT NULL
		   ,`cdept_id` INT NULL
		   ,`college_id` INT NULL
		   ,PRIMARY KEY (`id`)
		   ,INDEX `collegeid_idx` (`college_id` ASC) VISIBLE
		   ,INDEX `cdept_idx` (`cdept_id` ASC) VISIBLE,
            CONSTRAINT `cdept`
                FOREIGN KEY (`cdept_id`)
                 REFERENCES `education`.`college_department` (`cdept_id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION
				  ,CONSTRAINT `collegeid`
                       FOREIGN KEY (`college_id`)
                        REFERENCES `education`.`college` (`id`)
                         ON DELETE NO ACTION
                         ON UPDATE NO ACTION);

CREATE TABLE `education`.`syllabus` (
            `id` INT NOT NULL
		   ,`cdept_id` INT NULL
		   ,`syllabus_code` CHAR(4) NOT NULL
		   ,`syllabus_name` VARCHAR(100) NOT NULL
		   ,PRIMARY KEY (`id`)
		   ,INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE
		   ,CONSTRAINT `cdept_id`
                FOREIGN KEY (`cdept_id`)
                 REFERENCES `education`.`college_department` (`cdept_id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION);
				  
CREATE TABLE `education`.`semester_result` (
            `stud_id` INT NULL
		   ,`syllabus_id` INT NULL
		   ,`semester` TINYINT NOT NULL
		   ,`grade` VARCHAR(45) NOT NULL
		   ,`credits` FLOAT NOT NULL
		   ,`result_date` DATE NOT NULL
		   ,INDEX `stud_id_idx` (`stud_id` ASC) VISIBLE
		   ,INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE
		   ,CONSTRAINT `stud_id`
                FOREIGN KEY (`stud_id`)
                 REFERENCES `education`.`student` (`id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION
				  ,CONSTRAINT `syllabus_id`
                       FOREIGN KEY (`syllabus_id`)
                        REFERENCES `education`.`syllabus` (`id`)
                         ON DELETE NO ACTION
                         ON UPDATE NO ACTION);
						 
CREATE TABLE `education`.`employee` (
            `id` INT NOT NULL
		   ,`name` VARCHAR(100) NOT NULL
		   ,`dob` DATE NOT NULL
		   ,`email` VARCHAR(50) NOT NULL
		   ,`phone` BIGINT NOT NULL
		   ,`college_id` INT NULL
		   ,`cdept_id` INT NULL
		   ,`desig_id` INT NULL
		   ,PRIMARY KEY (`id`)
		   ,INDEX `collegeid_idx` (`college_id` ASC) VISIBLE
		   ,INDEX `cdeptid_idx` (`cdept_id` ASC) VISIBLE
		   ,INDEX `desigid_idx` (`desig_id` ASC) VISIBLE
		   ,CONSTRAINT `cid`
                FOREIGN KEY (`college_id`)
                 REFERENCES `education`.`college` (`id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION
				  ,CONSTRAINT `cdeptid`
                       FOREIGN KEY (`cdept_id`)
                        REFERENCES `education`.`college_department` (`cdept_id`)
                         ON DELETE NO ACTION
                         ON UPDATE NO ACTION
						 ,CONSTRAINT `desigid`
                              FOREIGN KEY (`desig_id`)
                               REFERENCES `education`.`designation` (`id`)
                                ON DELETE NO ACTION
                                ON UPDATE NO ACTION);
								
CREATE TABLE `education`.`professor_syllabus` (
            `emp_id` INT NULL
			,`syllabus_id` INT NULL
			,`semester` TINYINT NOT NULL
			,INDEX `emp_id_idx` (`emp_id` ASC) VISIBLE
			,INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE
			,CONSTRAINT `emp`
                 FOREIGN KEY (`emp_id`)
                  REFERENCES `education`.`employee` (`id`)
                   ON DELETE NO ACTION
                   ON UPDATE NO ACTION
				   ,CONSTRAINT `syllabus`
                        FOREIGN KEY (`syllabus_id`)
                         REFERENCES `education`.`syllabus` (`id`)
                          ON DELETE NO ACTION
                          ON UPDATE NO ACTION);
						  
CREATE TABLE `education`.`semester_fee` (
            `cdept_id` INT NULL
			,`stud_id` INT NULL
			,`semester` TINYINT NOT NULL
			,`amount` DOUBLE NULL
			,`paid_year` YEAR NULL
			,`paid_status` VARCHAR(10) NOT NULL
			,INDEX `stud_id_idx` (`stud_id` ASC) VISIBLE
			,INDEX `cd_id_idx` (`cdept_id` ASC) VISIBLE
			,CONSTRAINT `cd_id`
                 FOREIGN KEY (`cdept_id`)
                  REFERENCES `education`.`college_department` (`cdept_id`)
                   ON DELETE NO ACTION
                   ON UPDATE NO ACTION
				   ,CONSTRAINT `sd_id`
                        FOREIGN KEY (`stud_id`)
                         REFERENCES `education`.`student` (`id`)
                          ON DELETE RESTRICT
                          ON UPDATE CASCADE);