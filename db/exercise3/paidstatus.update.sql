UPDATE semester_fee
   SET paid_status = 'Paid'
      ,paid_year = '2020'
 WHERE stud_id = (SELECT id
                    FROM student
				   WHERE roll_number = '45');
                 
UPDATE semester_fee
   SET paid_status = 'Paid'
      ,paid_year = '2020'
 WHERE stud_id 
    IN (SELECT id
          FROM student
	     WHERE roll_number 
		    IN('18001'
			  ,'18002'
              ,'18003'
              ,'18007'));
