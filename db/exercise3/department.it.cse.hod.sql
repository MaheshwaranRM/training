SELECT college.code
      ,college.name
	  ,(SELECT DISTINCT university_name 
	      FROM exercise3.university 
		 WHERE university.univ_code = college.univ_code limit 1)
      ,college.city
      ,college.state
      ,college.year_opened
     ,(SELECT DISTINCT department.dept_name 
	     FROM exercise3.department 
		WHERE department.dept_name = 'CSE' 
		   OR department.dept_name = 'IT' 
		   OR department.univ_code = college.univ_code limit 1)
     ,(SELECT DISTINCT designation.name 
	     FROM exercise3.designation 
		WHERE designation.name = 'HOD') 
  FROM exercise3.college
      ,exercise3.university 
 WHERE college.univ_code = university.univ_code