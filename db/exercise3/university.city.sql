SELECT student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name` AS 'college_name'
      ,department.`dept_name` AS 'department_name'
      ,employee.`name` AS 'hod_name'
  FROM department
      ,student
      ,employee
      ,college_department
      ,college
      ,university
      ,designation
 WHERE college.univ_code = university.univ_code
   AND student.college_id = college.id 
   AND employee.college_id = college.id
   AND employee.desig_id = designation.id
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND college_department.college_id = college.id
   AND employee.desig_id = '2001'
   AND university.university_name = 'Anna University'
   AND college.city = 'Coimbatore'