SELECT designation.name 
    AS 'designation'
	,designation.rank
	,college.name 
	AS 'college_name'
	,department.dept_name
	,university.university_name
  FROM employee
      ,college_department
      ,department
	  ,university
	  ,designation
	  ,college
 WHERE employee.name is NOT NULL
   AND college.univ_code = university.univ_code
   AND department.univ_code = university.univ_code
   AND college_department.college_id = college.id
   AND employee.college_id = college.id
   AND employee.cdept_id = college_department.cdept_id
   AND employee.desig_id = designation.id
