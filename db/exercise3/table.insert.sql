

insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('1110','1046','KPR Institute','1000','Coimbatore','tamilnadu','1960');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('1111','2356','CIT','1000','Coimbatore','Tamilnadu','1969');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('1112','1356','Annamalai University','1003','Cuddalore','tamilnadu','2009');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('1113','4578','Sakthi Institute','1002','Coimbatore','tamilnadu','1984');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('1114','1234','Park Engineering College','1004','Coimbatore','tamilnadu','1998');



insert into `education`.`college_department`(cdept_id,udept_code,college_id)
values('3001','1010','1110');
insert into `education`.`college_department`(cdept_id,udept_code,college_id)
values('3002','2020','1111');
insert into `education`.`college_department`(cdept_id,udept_code,college_id)
values('3003','3030','1112');
insert into `education`.`college_department`(cdept_id,udept_code,college_id)
values('3004','4040','1113');
insert into `education`.`college_department`(cdept_id,udept_code,college_id)
values('3005','5050','1114');



insert into `education`.`student`(id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values('1','18001','Mahesh','2001-04-27','M','abc@gmail.com','1234567','barathi nagar','2020','3001','1110');
insert into `education`.`student`(id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values('2','18002','Praveen','2000-10-19','M','xyz@gmail.com','246810','annai nagar','2020','3004','1117');
insert into `education`.`student`(id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values('3','18003','Naveen','1999-01-25','M','idk@gmail.com','3691218','besant nagar','2020','3005','1113');
insert into `education`.`student`(id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values('4','18004','Venkat','2000-03-16','M','lmn@gmail.com','48121620','anna nagar','2020','3001','1116');
insert into `education`.`student`(id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values('5','18005','Prasath','2001-09-29','M','uvw@gmail.com','5101520','kandhan nagar','2020','3004','1114');


insert into education.university(univ_code,university_name) values('1000','ANNA');
insert into education.university(univ_code,university_name) values('1001','Annamalai');
insert into education.university(univ_code,university_name) values('1003','SASTRA');
insert into education.university(univ_code,university_name) values('1004','SRM');
insert into education.university(univ_code,university_name) values('1005','IIT');


 
insert into education.designation(id,name,grade) values ('2001','HOD','A');
insert into education.designation(id,name,grade) values ('2002','ASP','C');
insert into education.designation(id,name,grade) values ('2003','AP','B');



insert into `education`.`department`(dept_code,dept_name,univ_code)
values('1010','CSE','1000');
insert into `education`.`department`(dept_code,dept_name,univ_code)
values('2020','IT','1000');
insert into `education`.`department`(dept_code,dept_name,univ_code)
values('3030','ECE','1003');
insert into `education`.`department`(dept_code,dept_name,univ_code)
values('4040','EEE','1004');
insert into `education`.`department`(dept_code,dept_name,univ_code)
values('5050','BME','1005');


   
insert into  `education`.`employee`(id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('4001','Dilip','1994-04-04','sdc@gmail.com','2345678','1111','3002','2001');
insert into  `education`.`employee`(id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('4002','Raguvaran','1990-10-10','cat@gmail.com','9101112','1111','3001','2001');
insert into  `education`.`employee`(id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('4003','Naveen Kumar','1996-12-07','van@gmail.com','13141517','1110','3004','2003');
insert into  `education`.`employee`(id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('4004','Rajasekar','1996-09-01','guv@gmail.com','18171910','1114','3005','2005');
insert into  `education`.`employee`(id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('4005','Sudarshan','1989-12-26','tcs@gmail.com','2343409','1115','3001','2004');



   
insert into `education`.`professor_syllabus`(emp_id,syllabus_id,semester)
values('4001','6001','3');
insert into `education`.`professor_syllabus`(emp_id,syllabus_id,semester)
values('4002','6002','4');
insert into `education`.`professor_syllabus`(emp_id,syllabus_id,semester)
values('4003','6003','5');
insert into `education`.`professor_syllabus`(emp_id,syllabus_id,semester)
values('4004','6004','2');
insert into `education`.`professor_syllabus`(emp_id,syllabus_id,semester)
values('4005','6005','1');


insert into `education`.`semester_fee`(cdept_id,stud_id,semester,amount,paid_year,paid_status)
values('3001','1','3','80000','2020','paid');
insert into `education`.`semester_fee`(cdept_id,stud_id,semester,amount,paid_year,paid_status)
values('3002','2','5','100000','2020','notpaid');
insert into `education`.`semester_fee`(cdept_id,stud_id,semester,amount,paid_year,paid_status)
values('3003','3','2','85000','2020','notpaid');
insert into `education`.`semester_fee`(cdept_id,stud_id,semester,amount,paid_year,paid_status)
values('3004','4','1','50000','2020','paid');
insert into `education`.`semester_fee`(cdept_id,stud_id,semester,amount,paid_year,paid_status)
values('3005','5','4','65000','2020','notpaid');


   
insert into `education`.`syllabus` (id,cdept_id,syllabus_code,syllabus_name)
values ('6002','3002','fgg','engimaths');
insert into `education`.`syllabus`(id,cdept_id,syllabus_code,syllabus_name)
values ('6003','3003','dfg','engimaths2');
insert into `education`.`syllabus`(id,cdept_id,syllabus_code,syllabus_name)
values ('6004','3001','dfr','engigraphics');
insert into `education`.`syllabus`(id,cdept_id,syllabus_code,syllabus_name)
values ('6001','3004','afb','engichem');
insert into `education`.`syllabus`(id,cdept_id,syllabus_code,syllabus_name)
values ('6005','3005','dae','engiphy');


   
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,result_date)
values('1','6001','3','A','4','2020-01-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,result_date)
values('2','6002','2','B','3','2020-02-05');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,result_date)
values('3','6003','4','C','4','2020-03-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,result_date)
values('4','6004','1','A','1','2020-02-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,result_date)
values('5','6005','5','U','4','2020-01-05');
