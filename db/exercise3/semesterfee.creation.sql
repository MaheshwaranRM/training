CREATE TABLE semester_fee(
    cdept_id int(4)
   ,stud_id int(4)
   , semester int(2)
   ,amount int(5)
   ,paid_year year
   ,paid_status varchar(6));
ALTER TABLE semester_fee ADD COLUMN amount int(5);
ALTER TABLE semester_fee ALTER amount SET DEFAULT '50000'; 
ALTER TABLE semester_fee MODIFY COLUMN paid_year varchar(5)
ALTER TABLE semester_fee ALTER paid_year SET DEFAULT 'NULL'; 
ALTER TABLE semester_fee ADD COLUMN paid_status varchar(6)
ALTER TABLE semester_fee ALTER paid_status SET DEFAULT 'Unpaid'; 
INSERT INTO semester_fee(cdept_id,stud_id,semester) VALUES('3001','1','5')
INSERT INTO semester_fee(cdept_id,stud_id,semester) VALUES('3002','2','4')
INSERT INTO semester_fee(cdept_id,stud_id,semester) VALUES('3003','3','1')
INSERT INTO semester_fee(cdept_id,stud_id,semester) VALUES('3004','4','2')
