SELECT student.`id` 
    AS 'student_id'
      ,student.`roll_number`
      ,student.`name` 
	AS 'student_name'
      ,student.`gender`
      ,college.`code` 
	AS 'college_code'
      ,college.`name` 
	AS 'college_name'
      ,semester_result.`semester`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id` = college.`id`
   AND semester_result.`stud_id` = student.`id`
   AND semester_result.`gpa`>8
   AND semester_result.semester = '3';

SELECT student.`id` 
    AS 'student_id'
      ,student.`roll_number`
      ,student.`name` 
	AS 'student_name'
      ,student.`gender`
      ,college.`code` 
	AS 'college_code'
      ,college.`name` 
	AS 'college_name'
      ,semester_result.`semester`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id` = college.`id`
   AND semester_result.`stud_id` = student.`id`
   AND semester_result.`gpa`>5
   AND semester_result.semester = '4';