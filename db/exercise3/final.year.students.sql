SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,(SELECT DISTINCT college.name 
          FROM exercise3.college 
		 WHERE student.college_id = college.id) 
            AS college_name
	  ,(SELECT DISTINCT department.dept_name 
          FROM exercise3.department 
		 WHERE department.dept_code = student.cdept_id) 
            AS department_name 
  FROM exercise3.student 
 WHERE student.academic_year = '2019' 
   AND student.cdept_id 
    IN(SELECT department.dept_code 
         FROM exercise3.department 
		WHERE department.dept_code 
           IN('1010','2020','3030','4040','5050'))
   AND student.college_id 
    IN(SELECT DISTINCT university.univ_code 
         FROM exercise3.university 
		WHERE university.univ_code = '1000')
   AND student.address = 'Coimbatore' 
 ORDER BY student.name