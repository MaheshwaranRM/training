/*
 Requirements:
    To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL.
        
 Entities:
    DateLocaleFormat
   
 Function Declaration:
    public static void main(String[] args)
   
 Jobs to be done:
    1.create a DateFormat using the getDateInstance() method of the DateFormat class.
    2.Print the Date formats
        2.1)Set default parameter ,print default date
        2.2)Set short parameter ,print short format date
        2.3)Set medium parameter ,print medium format date
        2.4)Set long parameter ,print long format date
        2.5)Set full parameter ,print full format date
     
Pseudo Code:

public class DateLocaleFormat {

	public static void main(String[] args) {
	
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.LONG);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.FULL);

	    System.out.println(dateFormat.format(new Date()));
	        
	}

}
              
 */
package com.java.core.internationalization;

import java.text.DateFormat;
import java.util.Date;

public class DateLocaleFormat {

	public static void main(String[] args) {
	
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.LONG);

	    System.out.println(dateFormat.format(new Date()));

	    dateFormat = DateFormat.getDateInstance(DateFormat.FULL);

	    System.out.println(dateFormat.format(new Date()));
	        
	}

}
