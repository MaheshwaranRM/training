/*
Requirements:
    To write a java code to print my age
    
Entities:
    AgeCalculation
    
Function declaration:
    public static void man(String[] args)
    
Jobs to be done:
    1)Create an object for LocalDate named dateOfBirth and store the date of birth.
    2)Create an object for LocalDate named localDate and store the Current date.
    3)Create an instance for Period as age and calculate difference between the dateOfBirth and
       current date that is stored in age u
    3. Print the age.
    
Pseudo code:

public class CalculateAge {
    
    public static void main(String[] args) {
        
    	LocalDate dateOfBirth = LocalDate.of(2001, 5, 14);
        LocalDate localDate = LocalDate.now();
        Period age = Period.between(dateOfBirth, localDate);
        System.out.println("Age:"+age.getYears());
    }
}
*/

package com.java.core.datetime;

import java.time.LocalDate;
import java.time.Period;

public class CalculateAge {
    
    public static void main(String[] args) {
        
    	LocalDate dateOfBirth = LocalDate.of(2001, 5, 14);
        LocalDate localDate = LocalDate.now();
        Period age = Period.between(dateOfBirth, localDate);
        System.out.println("Age:"+age.getYears());
    }
}