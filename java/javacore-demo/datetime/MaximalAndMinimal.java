/*
Requirements:
   Write a Java program to get the maximum and minimum value
   of the year, month, week, date from the current date of a default calendar.
    
Entities:
    MaximumAndMinimumDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1.  Create an instance of a Calendar named calendar that gets the calendar using current time zone
    2.  Create a variable maximumYear of type integer and store the maximum year of the default calendar.
    3.  Print the maximumYear.
    4.  Create a variable maximumMonth of type integer and store the maximum month of the default 
        calendar.
    5.  Print the maximumMonth.
    6.  Create a variable maximumWeek of type integer and store the maximum week of the default 
        calendar.
    7.  Print the maximumWeek.
    8.  Create a variable maximumDay of type integer and store the maximum day of the default 
        calendar.
    9.  Print the maximumDay.
    10. Create a variable minimumYear of type integer and store the minimum year of the default calendar.
    11. Print the minimumYear.
    12. Create a variable miniMonth of type integer and store the minimum month of the default 
        calendar.
    13. Print the minimumMonth.
    14. Create a variableminimumWeek of type integer and store the minimum week of the default 
        calendar.
    15. Print the minimumWeek.
    16. Create a variable minimumDay of type integer and store the minimum day of the default 
        calendar.
    17. Print the minimumDay.    
 
 Pseudo code:
 public class MaximalAndMinimal {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTime());

		int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
		System.out.println(maximumYear);

		int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
		System.out.println("The maximum month of default calendar is " + " " + maximumMonth);

		int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		System.out.println(maximumWeek);

		int maximumDay = calendar.getActualMaximum(Calendar.DATE);
		System.out.println(maximumDay);

		int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
		System.out.println(minimumYear);

		int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
		System.out.println(minimumMonth);

		int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		System.out.println( minimumWeek);

		int minimumDay = calendar.getActualMinimum(Calendar.DATE);
		System.out.println(minimumDay);
	}
 }
*/

package com.java.core.datetime;

import java.util.Calendar;


public class MaximalAndMinimal {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();
		System.out.println("The current date is " + " " + calendar.getTime());

		int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
		System.out.println("The maximum year of default calendar is " + maximumYear);

		int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
		System.out.println("The maximum month of default calendar is " + " " + maximumMonth);

		int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		System.out.println("The maximum week of default calendar is " + maximumWeek);

		int maximumDay = calendar.getActualMaximum(Calendar.DATE);
		System.out.println("The maximum day of default calendar is " + maximumDay);

		int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
		System.out.println("The minimum year of the default calendar is " + minimumYear);

		int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
		System.out.println("The minimum month of the default calendar is " + minimumMonth);

		int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		System.out.println("The minimum week of the default calendar is " + minimumWeek);

		int minimumDay = calendar.getActualMinimum(Calendar.DATE);
		System.out.println("The minimum day of the default calendar is " + minimumDay);
	}
}