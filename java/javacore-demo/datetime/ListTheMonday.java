/*
Requirements:
    To code for list of all the Mondays in the current year of the given month.
    
Entities:
    ListTheMonday
    
Function Declaration:
    public static void main(String[] args) 
    
Jobs to be done:
    1) Get the month from the user for which list of mondays to be printed and store it in String
       variable givenMonth.
    2) Create a reference for Month as month.
     	2.1)Use the toUppercase() method to convert the given month into uppercase and store it in month.
    3) A reference is created for LocalDatae as localDate.
    	3.1) The object localDate stores the date of first Monday in that month of the current year.
    4) Now create an another instance for Month as newMonth which stores the date of the given month.
    5) Check whether the newMonth and month are equal,
        5.1) if condition satisfies print the date.
                    5.1.1) get the next mondays date and print it


 Pseudo code:
 class ListTheMondays {
     
     public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month to which list of Mondays to find: ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.Monday));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.Monday));
            newMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
*/

package com.kpr.training.dateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListTheMonday {
    
    public static void main(String[] args) {
        
    	Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the month : ");
        String givenMonth = scanner.nextLine();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.printf("For the given month\n", month);
        
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                              .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            
        	System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            newMonth = localDate.getMonth();
        }
        scanner.close();
    }
}