/*
Requirements: 
    To find the whether the given date has eleventh of the month as tuesday.
  
Entities: 
    TuesdayEleventh
  
Function Declaration: 
    public static void main(String[] args);
  
Jobs to be done:
  	1. Assign the input date to date variable which is of type LocalDate.
  	2.Check if the day  of the month in the given date is 11 and day of week is tuesday.
  		2.1) Print "The given date is 11 and day is tuesday.
   	3. Check if day of month is 11
  		3.1) Print the day is 11.
  	4. Check if day of week is tuesday.
  		4.1) Print the day of week is tuesday.
  	5. Print Wrong day of week and day of month.
    
PseudoCode:
  
public class PreviousFriday {
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2015, 8, 11);
		if((date.get(ChronoField.DAY_OF_MONTH) == 11) &&
           (date.get(ChronoField.DAY_OF_WEEK) == 2)) {
			System.out.println("Given date is 11 and day is tuesday");
		}
		
		else if(date.get(ChronoField.DAY_OF_MONTH) == 11) {
			System.out.println("Given date is 11 ");
		}
		
		else if(date.get(ChronoField.DAY_OF_WEEK) == 2) {
			System.out.println("Day is tuesday");
		}
		
		else {
			System.out.println("Wrong day of week and day of month");
		}
	}
}

*/
package com.java.core.datetime;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class TuesdayEleventh {

	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2015, 8, 11);
		if((date.get(ChronoField.DAY_OF_MONTH) == 11) &&
		   (date.get(ChronoField.DAY_OF_WEEK) == 2)) {
			System.out.println("Given date is 11 and day is tuesday");
		}
	
		else if(date.get(ChronoField.DAY_OF_MONTH) == 11) {
			System.out.println("Given date is 11 ");
		}
	
		else if(date.get(ChronoField.DAY_OF_WEEK) == 2) {
			System.out.println("Day is tuesday");
		}
	
		else {
			System.out.println("Wrong day of week and day of month");
		}
	}
}