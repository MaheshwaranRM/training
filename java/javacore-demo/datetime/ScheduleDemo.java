/* 
Requirements:
    To code for the schedule() method in Timer class.

Entities:
    ScheduleMethodDemo

Function Declaration:
    public static void main(String[] args)
    public void run()
    
Jobs to be done:
    1)An object is created for Timer named timer.
    2)An object is created for TimerTask named timerTask
    3)For each number , from 0 to 10 
           3.1) Print that number
           3.2)cancel that timer.
    4)The task is schedule to execute at 1000ms and the time between the successive task is set as 10ms.
    
Pseudo code:

public class ScheduleDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int number = 0; number <= 10; number++ ) {
                    System.out.println( "number : " + number);
                    timer.cancel();
                }   
            }
        };
        timer.schedule(task, 1000, 10);
    }
}

*/

package com.java.core.datetime;

import java.util.Timer;
import java.util.TimerTask;

public class ScheduleDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int number = 0; number <= 10; number++ ) {
                    System.out.println( "number : " + number);
                    timer.cancel();
                }   
            }
        };
        timer.schedule(task, 1000, 10);
    }
}