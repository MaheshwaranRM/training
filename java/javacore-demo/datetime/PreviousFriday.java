/*
Requirements: 
    To find the previous friday when given with a date.
  
Entities: 
    PreviousFriday
  
Function Declaration: 
    public static void main(String[] args);
  
Jobs to be done:
  	1. Assign the input date to date variable which is of type LocalDate.
  	2. Find the previousFriday from the given date.
  	3. print the previousFriday.
  
PseudoCode:
  
public class PreviousFriday {
	
	public static void main(String[] args) {
		
		LocalDate date = 2020-10-02; 
		System.out.printf("The previous Thursday is: ", previous friday from date));
	}
}

*/

package com.java.core.datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.*;

public class PreviousFriday {
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2020, 10, 1);
		System.out.printf("The previous Thursday is: %s%n", date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
	}
}