/*
Requirements:
	How do you convert a Calendar to Date and vice-versa with example?
 
Entities:
	DateAndCalendar

Function Declaration:
	public static void main(String[] args) {}

Jobs To Be Done:
   1) Create an instance for Date class and pass the milliseconds as parameter.
   2) Invoke the getInstance() method to get current time.
   3) Invoke the method setTime() and pass the date.
   4) Invoke the method getTime() and print.
   5) Invoke the getInstance() method to get current time.
   6) Invoke the getTime() method and store in variable date1 type Date.
   7) Print the date1.
   
Pseudo Code:

public class DateAndCalendar {

	public static void main(String[] args) {
		
		Date date = new Date(0);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println(calendar.getTime());
		
		Calendar calendar1 = Calendar.getInstance();
		Date date1 = calendar1.getTime();
		System.out.println(date1);
	}
}
 */
package com.java.core.datetime;

import java.util.Calendar;
import java.util.Date;

public class DateAndCalendar {

	public static void main(String[] args) {
		
		Date date = new Date(0);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println(calendar.getTime());
		
		Calendar calendar1 = Calendar.getInstance();
		Date date1 = calendar1.getTime();
		System.out.println(date1);
	}
}