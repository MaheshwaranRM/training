/*
Requirements:
    To find the difference between the times using timer class.
    
Entities:
    TimeDifference
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1.Declare a variable named startTime of type long and store the current time in milliseconds
    2.Print the Start time.
    3.For each number till 100
      3.1.Print the number
    4.Declare a variable named endTime of type long and store the current time in millisecond.	
    5.Print the difference between start time and the end time in ms.
    
Pseudo code:

public class TimeDifference {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis(); 
		System.out.println(startTime);

		for (int number = 0; number < 100; number++) {
			System.out.println(number);
		}

		long endTime = System.currentTimeMillis(); 
		System.out.println(endTime);
		System.out.println(endTime - startTime");
	} 
}
*/

package com.java.core.datetime;

public class TimeDifference {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis(); 
		System.out.println("staring time in millsecond  :  " + startTime + "ms");

		for (int number = 0; number < 100; number++) {
			System.out.println(number);
		}

		long endTime = System.currentTimeMillis(); 
		System.out.println("End  time in millsecond :  " + endTime + "ms");
		System.out.println("Time difference : " + (endTime - startTime) + "ms"); 
	} 
}