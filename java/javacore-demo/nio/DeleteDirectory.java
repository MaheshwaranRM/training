/*
Requirements:
	Delete the directory along with the files recursively.
	
Entities:
	DeleteDirectory
	
Function Declaration:
	public static void main(String[] args) {}
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {}
	
Jobs To Be Done:
	1) Create a path instance for a rootPath.
    2) By using walkfileTree method extends simpleFileVisitor.
    3) In a postVisitDirectory() method traverse the root path.
    	3.1) Invoke delete() method and delete the directory.
    4) Print the deleted directory path.
    5) The operation continue until when the root path does not have any directories.
	
Pseudo code:

public class DeleteDirectory {
	
	public static void main(String[] args) {
		
		Path rootPath = Paths.get("C:\\1dev\\java\\source.txt");

		try {
			Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
		    
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					
					Files.delete(dir);
					System.out.println("Delete directory: " + dir.toString());
					return FileVisitResult.CONTINUE;
				}
			});
		} 
		catch(IOException e){
		  e.printStackTrace();
		}
	}
}
*/

package com.java.core.nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;

public class DeleteDirectory {
	
	public static void main(String[] args) {
		
		Path rootPath = Paths.get("C:\\1dev\\java\\source.txt");

		try {
			Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
		    
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					
					Files.delete(dir);
					System.out.println("Delete directory: " + dir.toString());
					return FileVisitResult.CONTINUE;
				}
			});
		} catch(IOException e) {
		    e.printStackTrace();
		}
		
	}
	
}
