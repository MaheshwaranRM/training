/*
Requirements:
    To create two files named source and destination .
    Transfer the data from source file to destination file using NIO file channel.
    
Entities:
    FileTransferDemo
    
Function Declaration:
     public static void main(String[] args) throws Exception{}
     
Jobs to be done:
    1) Declare a variable file type String and store the path of source.txt file.
    2) Declare a variable file2 type String and store the path of destination.txt file.
    3) Create an object for RandomAccessFile as sourceFile and pass the file in rw (read write) mode.
    4) Create a reference for FileChannel as sourceChannel
    5) Invoke getChannel() method and get the file data to the channel.
    6) Create a reference for FileChannel as destinationChannel
    7) Invoke getChannel() method and get the file data to the channel.
    8) Transfer the data from source to destination using file channel.
    9) Print the message.
    
Pseudo code:

public class FileDataTransfer {
	
    public static void main(String[] args) throws Exception {
    	
    	String file = "C:\0dev\training\javacore -demo\nio exercise\nio\source.txt";
    	String file2 = "C:\0dev\training\javacore -demo\nio exercise\nio\destination.txt";
    	
        RandomAccessFile sourceFile = new RandomAccessFile(file,"rw");
        FileChannel sourceChannel = sourceFile.getChannel();

        RandomAccessFile destinationFile = new RandomAccessFile(file2,"rw");
        FileChannel destinationChannel = destinationFile.getChannel();

        long position = 0;
        long count = sourceChannel.size();

        destinationChannel.transferFrom(sourceChannel, position, count);
        System.out.println("Data Transferred to Destination file");
        sourceFile.close();
        destinationFile.close();
    }
} 

*/

package com.java.core.nio;

import java.nio.channels.FileChannel;
import java.io.RandomAccessFile;

public class FileDataTransfer {
	
    public static void main(String[] args) throws Exception {
    	
    	String file = "C:\\0dev\\training\\javacore -demo\\nio exercise\\nio\\source.txt";
    	String file2 = "C:\\0dev\\training\\javacore -demo\\nio exercise\\nio\\destination.txt";
    	
        RandomAccessFile sourceFile = new RandomAccessFile(file,"rw");
        FileChannel sourceChannel = sourceFile.getChannel();

        RandomAccessFile destinationFile = new RandomAccessFile(file2,"rw");
        FileChannel destinationChannel = destinationFile.getChannel();

        long position = 0;
        long count = sourceChannel.size();

        destinationChannel.transferFrom(sourceChannel, position, count);
        System.out.println("Data Transferred to Destination file");
        sourceFile.close();
        destinationFile.close();
   
    }

}
