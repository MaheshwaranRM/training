/*
Requirements:
	Delete a file using visitFile() method.
	
Entities:
	DeleteFile
	
Function Declaration:
	public static void main(String[] args) {}
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {}

Jobs To Be Done:
	1) Create a path instance for a rootPath.
    2) By using walkfileTree method extends simpleFileVisitor.
    3) In a visitFile() method and traverse the root path.
    	3.1) Invoke delete() method and delete the file.
    4) Print the deleted directory file.
    5) The operation continue until when the root path does not have any files.
    
Pseudo code:

public class DeleteFile {
	
	public static void main(String[] args) {
		
		Path rootPath = Paths.get("C:\\1dev\\java\\source.txt");

		try {
			Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
		    
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					
					System.out.println("Delete file: " + file.toString());
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}
			});
		} 
		catch(IOException e){
		  e.printStackTrace();
		}
	}
}
*/

package com.java.core.nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteFile {
	
	public static void main(String[] args) {
		
		Path rootPath = Paths.get("C:\\1dev\\java\\source.txt");

		try {
			Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
		    
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					
					System.out.println("Delete file: " + file.toString());
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}
			});
		} 
		catch(IOException e){
		  e.printStackTrace();
		}
		
	}
	
}
