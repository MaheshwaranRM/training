/*
Requirements:
    To write a program to write and read the string to a channel using buffer(to demonstrate Pipe)
    
Entities:
    NioPipe
    
Function Declaration:
    public static void main(String[] args) throws IOException {}
    
Jobs to be done:
	1) Declare a variable sentence type String and store the value.
    1) Create an instance for Pipe and invoke open() method.
    2) Create a sink channel to write data to the pipe.
        2.1) Create a reference for ByteBuffer as buffer and allocate a 128 bytes.
        2.2) Add the data to the buffer.
        2.3) Write the data to the sink channel.
    3) Create a source channel to read data from the pipe.
        3.1) Allocate a memory to a buffer.
        3.2) Read the data through source channel.
        3.3) Print the data.
        
Pseudo code:

public class NioPipe {
	
    public static void main(String[] args) throws IOException {
    	
    	String sentence = "Java is a class-based, object-oriented programming language";
    	
        Pipe pipe = Pipe.open();
        Pipe.SinkChannel sinkChannel = pipe.sink();
        
        ByteBuffer buffer = ByteBuffer.allocate(128);
        
        buffer.clear();
        buffer.put(sentence.getBytes());
        buffer.flip();
        
        while (buffer.hasRemaining()) {
        	
            sinkChannel.write(buffer);
        }
        
        Pipe.SourceChannel sourceChannel = pipe.source();
        
        buffer = ByteBuffer.allocate(128);
        
        while (sourceChannel.read(buffer) > 0) {
        	
            buffer.flip();
            
            while (buffer.hasRemaining()) {
            	
                char data = (char) buffer.get();
                System.out.print(data);
            }
            buffer.clear();
        }
    }
}
*/

package com.java.core.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

public class NioPipe {
	
    public static void main(String[] args) throws IOException {
    	
    	String sentence = "Java is a class-based, object-oriented programming language";
    	
        Pipe pipe = Pipe.open();
        Pipe.SinkChannel sinkChannel = pipe.sink();
        
        ByteBuffer buffer = ByteBuffer.allocate(128);
        
        buffer.clear();
        buffer.put(sentence.getBytes());
        buffer.flip();
        
        while (buffer.hasRemaining()) {
        	
            sinkChannel.write(buffer);
        }
        
        Pipe.SourceChannel sourceChannel = pipe.source();
        
        buffer = ByteBuffer.allocate(128);
        
        while (sourceChannel.read(buffer) > 0) {
        	
            buffer.flip();
            
            while (buffer.hasRemaining()) {
            	
                char data = (char) buffer.get();
                System.out.print(data);
            }
            buffer.clear();
        }
        
    }
    
}
