/*1.
Requirement: 
    To demonstrate establish the difference between lookingAt() and matches().
	To find the no of occurrence of a pattern in the text and also fetch the start and end index of the the occurrence.

Entity:
    LookingAtMatches

Function declaration: 
    public static void main(String[] args)

Jobs to be done:
    1. Create a pattern to be compiled or searched for.
  	2. Save the string in which the pattern has to be saved in match.
  	3. Initialise the count value to 0.
  	4. While the match is found
  		4.1) Print the pattern is found. 
  		4.2) Print the start and the end index.
  		4.3) Increment the count by 1.
    5. Print the count value.
  	6. Check if the pattern is found the string using matches().
  	7. Check if the pattern is found using lookingAt();
 
PseudoCode:
 
public class LookingAtMatches { 
	
    public static void main(String[] args) {
        // Create a pattern to be searched 
        Pattern pattern = Pattern.compile("the"); 
        Matcher match = pattern.matcher("He spilled the milk all over the floor");        
       	int count = 0;
        
        while (match.find()) {
            System.out.println("Pattern found from " + match.start() + " to " + match.end()); 
            count = count + 1;
        }
        
        System.out.println("Occurence of pattern is: " + count);
        //If the regular expression matches the whole text, then the matches() method returns true. 
        //If not, the matches() method returns false.

        Pattern pattern1 = Pattern.compile(".the." );
        Matcher match1 = pattern1.matcher("The Name of the tallest building in the world is Burj Khalifa");   
        System.out.println(match1.matches());
        
        System.out.println("Using looking at");
        System.out.println("lookingAt(): " + match1.lookingAt());
        //if the regular expression matches the beginning of a text but not the whole text, 
        // lookingAt() will return true, whereas matches() will return false. 
    } 
}    

 */	 
package com.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LookingAtMatches { 
	
    public static void main(String[] args) {
        // Create a pattern to be searched 
        Pattern pattern = Pattern.compile("the"); 
        Matcher match = pattern.matcher("He spilled the milk all over the floor");        
        int count = 0;
        
        while (match.find()) {
            System.out.println("Pattern found from " + match.start() + " to " + match.end()); 
            count = count + 1;
        }
        
        System.out.println("Occurence of pattern is: " + count);
        System.out.println(match.matches());
        //If the regular expression matches the whole text, then the matches() method returns true. 
        //If not, the matches() method returns false.

        Pattern pattern1 = Pattern.compile(".the." );
        Matcher match1 = pattern1.matcher("The Name of the tallest building in the world is Burj Khalifa");   
        System.out.println(match1.matches());
        
        System.out.println("Using looking at");
        System.out.println("lookingAt(): " + match1.lookingAt());
        //if the regular expression matches the beginning of a text but not the whole text, 
        // lookingAt() will return true, whereas matches() will return false. 
    } 
}