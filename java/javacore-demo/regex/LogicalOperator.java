/*
 Requirements:
  1.)Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product 
     input by the user.
     The discount rate is 10% for the quantity purchased between 100 and 120 units, 
     and 15% for the quantity purchased greater than 120 units. 
     If the quantity purchased is less than 100 units, the discount rate is 0%.
     See the example output as shown below:
        Enter unit price: 25
        Enter quantity: 110
        The revenue from sale: 2475.0$
        After discount: 275.0$(10.0%)
 
 Entities:
   - LogicalOperator
  
 Function Declaration:
   - public static void main(String[] args)  
   - static void calculateSale()
   
 Jobs to be done:
   1.Create a calculateSale() method.
   2.Initialize and declare the value of unit price ,quantity ,revenue and discount_rate with type of float and int.
   3.Check the condition 
     3.1)Check if quantity < 100 print revenue
     3.2)Check else if quantity >= 100 && quantity <= 120 print revenue
     3.3)Check else if quantity > 120 print revenue.
   4.Print the unit price ,quantity ,revenue ,discount_amount and discount_rate.
   
Pseudo Code:
public class LogicalOperator {
	
    public static void main(String[] args) {
        calculateSale();
    }

    static void calculateSale() {

        float unitprice = 0f;
        int quantity = 0;
        float revenue = 0f;
        float discount_rate = 0f, discount_amount = 0f;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter unit price : ");
        unitprice = sc.nextFloat();
        System.out.println("Enter quantity : ");
        
        quantity = sc.nextInt();
        if(quantity < 100) {
           revenue = unitprice * quantity;
        } else if(quantity >= 100 && quantity <= 120) {
           discount_rate = (float) 10 / 100;
           revenue = unitprice * quantity;
           discount_amount = revenue * discount_rate;
           revenue -= discount_amount;
        } else if(quantity > 120) {
           discount_rate = (float) 15 / 100;
           revenue=unitprice * quantity;
           discount_amount = revenue * discount_rate;
           revenue -= discount_amount;
        }
        System.out.println("The revenue from sale : " + revenue + "$");
        
    System.out.println("After discount : " + discount_amount + "$("+ discount_rate * 100 + "%)");
    }

}
            
 */

package com.java.core.regex;

import java.util.*;

public class LogicalOperator {
	
    public static void main(String[] args) {
        calculateSale();
    }

    static void calculateSale() {

        float unitprice = 0f;
        int quantity = 0;
        float revenue = 0f;
        float discount_rate = 0f, discount_amount = 0f;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter unit price : ");
        unitprice = sc.nextFloat();
        System.out.println("Enter quantity : ");
        
        quantity = sc.nextInt();
        if(quantity < 100) {
           revenue = unitprice * quantity;
        } else if(quantity >= 100 && quantity <= 120) {
           discount_rate = (float) 10 / 100;
           revenue = unitprice * quantity;
           discount_amount = revenue * discount_rate;
           revenue -= discount_amount;
        } else if(quantity > 120) {
           discount_rate = (float) 15 / 100;
           revenue=unitprice * quantity;
           discount_amount = revenue * discount_rate;
           revenue -= discount_amount;
        }
        System.out.println("The revenue from sale : " + revenue + "$");
        
    System.out.println("After discount : " + discount_amount + "$("+ discount_rate * 100 + "%)");
    }

}

