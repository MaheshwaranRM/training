package com.java.core.regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OccurencePattern {
	
	public static void main(String[] args) throws IOException {
		FileReader r = new FileReader("C:\\Users\\santh\\Desktop\\Training Wrk\\java wrk\\javaee-demo\\collections.txt");           
		BufferedReader bfr = new BufferedReader(r);
		String x="L[a-z]{2}y";
		String Y="";
		int count = 0;
		while ((Y=bfr.readLine())!=null)
		{
		    String[] words = Y.split(" ");
		    Pattern p = Pattern.compile(x);
		    for (String word : words) {
		        Matcher m = p.matcher(word);
				if(m.find())
		            count++;
		    }
		} 
	}
}
