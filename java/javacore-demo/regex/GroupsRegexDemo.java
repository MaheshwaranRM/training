/*1.
Requirements:
    To  demonstrate the different types of groups in regex.

Entity:
    GroupsRegexDemo
  
Function declaration: 
    public static void main(String[] args);
  
Jobs to be done:
    1.Initialise the text with string to be checked in.
  	2. Initialise the groups for the pattern to be checked as patternCheck, patternCheck1, patternCheck2.
  	3. Create a pattern object with the created groups.
  	4. Create a matcher for the text with the pattern.
  	5. Check for the occurence of the group in text and print the group.
  	6. Repeat the same for mutliple groups.
  					
 
PseudoCode:
 public class GroupsRegexDemo {
	
	public static void main(String[] args) {
        String text = "The Name of the tallest building in the world is Burj Khalifa";
        String patternCheck = "(the) ";
        String patternCheck1 = "(the)(.+?) ";
        String patternCheck2 = "((the)(.+?)) ";
        
        Pattern pattern = Pattern.compile(patternCheck ); 
        Matcher match = pattern.matcher(text);        
        System.out.println("For group (the)");
        while(match.find()) {
            System.out.println("found: " + match.group(1));
        }
        
        Pattern pattern1 = Pattern.compile(patternCheck1 ); 
        Matcher match1 = pattern1.matcher(text);        
        System.out.println("For group (the)(.+?)");
        while(match1.find()) {
            System.out.println("found: " + match1.group(1));
            System.out.println("found: " + match1.group(2));
        }
        
        Pattern pattern2 = Pattern.compile(patternCheck2 ); 
        Matcher match2 = pattern2.matcher(text);        
        System.out.println("For group ((the)(.+?))");
        while(match2.find()) {
            System.out.println("found: " + match2.group(1));
        }
         
    } 
}
 */	
package com.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupsRegexDemo {
	
	public static void main(String[] args) {
        String text = "The Name of the tallest building in the world is Burj Khalifa";
        String patternCheck = "(the) ";
        String patternCheck1 = "(the)(.+?) ";
        String patternCheck2 = "((the)(.+?)) ";
        
        Pattern pattern = Pattern.compile(patternCheck ); 
        Matcher match = pattern.matcher(text);        
        System.out.println("For group (the)");
        while(match.find()) {
            System.out.println("found: " + match.group(1));
        }
        
        Pattern pattern1 = Pattern.compile(patternCheck1 ); 
        Matcher match1 = pattern1.matcher(text);        
        System.out.println("For group (the)(.+?)");
        while(match1.find()) {
            System.out.println("found: " + match1.group(1));
            System.out.println("found: " + match1.group(2));
        }
        
        Pattern pattern2 = Pattern.compile(patternCheck2 ); 
        Matcher match2 = pattern2.matcher(text);        
        System.out.println("For group ((the)(.+?))");
        while(match2.find()) {
            System.out.println("found: " + match2.group(1));
        }
         
    } 
}