/*
Requirements: 
    To check whether an password is valid or not.

Entities: 
    PasswordCollections
 
Function Declaration : 
    public static void main(String[] args);
 
Jobs To be Done: 
    1. Get input from the user. 
  	2. Check if the input is null.
  		2.1. print the string cannot be null.
 	3. Create a regex pattern for valid password
  	4. Compile pattern.
  	5. check if it matches with the pattern
  		5.1 Print true or false.
 
PseudoCode:

public class PasswordCollections {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		String pass = scanner.nextLine();
		if(pass == null) {
			System.out.println("input is empty");
		}
		String regex = "^(?=.[0-9])" + "(?=.[a-z])(?=.*[A-Z]).{8,15}$";
		
		Pattern pattern = Pattern.compile(regex);
		Matcher match = pattern.matcher(pass);
		
		System.out.println(match.matches());
		scanner.close();
	}

}

 * 
 */
package com.java.core.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordCollections {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		String pass = scanner.nextLine();
		if(pass == null) {
			System.out.println("input is empty");
		}
		String regex = "^(?=.[0-9])" + "(?=.[a-z])(?=.*[A-Z]).{8,15}$";
		
		Pattern pattern = Pattern.compile(regex);
		Matcher match = pattern.matcher(pass);
		
		System.out.println(match.matches());
		scanner.close();
	}

}