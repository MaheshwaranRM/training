/*1.
Requirements: 
    To demonstrate establish the difference between replaceAll() and appendReplacement.

Entities:
    ReplceAnAppend
 
Function declaration: 
    public static void main(String[] args);
 
Jobs to be done:
    1. Create a pattern to be compiled or searched for in pattern..
 	2. Save the string in which the pattern has to be saved in matcher.
  	3. Create a object for StringBuffer.
  	4. While match is found the matcher
  		4.1) Append and replace the string "milk" to "water" and store to stringBuffer.
  		4.2) Print the value in StringBuffer by converting to string.
  	5. Append the tail of the string.
  	6. Replace the string "water " to honey using ReplaceAll.
   7. Print the string.
 
PseudoCode:

public class ReplaceAndAppend {
	
    public static void main(String[] args) {
        String text = "He spilled the milk all over the floor";
        String patternString1 = "(milk) ";

        Pattern pattern = Pattern.compile(patternString1);
        Matcher matcher = pattern.matcher(text);
        StringBuffer stringBuffer = new StringBuffer();

        while(matcher.find()){
            matcher.appendReplacement(stringBuffer, "water ");
            System.out.println(stringBuffer.toString());
        }
        matcher.appendTail(stringBuffer);

        System.out.println(stringBuffer.toString());
        
        String afterReplaceAll = matcher.replaceAll("honey ");
        System.out.println(afterReplaceAll);
    }
} 

 */	 
package com.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceAndAppend {
	
    public static void main(String[] args) {
        String text = "He spilled the milk all over the floor";
        String patternString1 = "(milk) ";

        Pattern pattern = Pattern.compile(patternString1);
        Matcher matcher = pattern.matcher(text);
        StringBuffer stringBuffer = new StringBuffer();

        while(matcher.find()){
            matcher.appendReplacement(stringBuffer, "water ");
            System.out.println(stringBuffer.toString());
        }
        matcher.appendTail(stringBuffer);

        System.out.println(stringBuffer.toString());
        
        String afterReplaceAll = matcher.replaceAll("honey ");
        System.out.println(afterReplaceAll);
    }
}