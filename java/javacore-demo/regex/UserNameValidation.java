/*
3.)create a username for login website which contains
   8-12 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit


Requirement :
	 Create a username for login website which contains
	   8-12 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit

Entities :
	UserNameValidation
	
Job to be Done :
	1.Create class UserNameValidation and main method.
	2.Create string userName.
	3.Using pattern class and enter the condition for userName in compile method.
	4.Using matcher class, check wheather pattern class matches the userName.
	5.If matcher find print it is valid username and else not valid.
*/


package com.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameValidation {
	 
    public static void main(String[] args) {
        
        String userName = "Username12";
        Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
        Matcher m = p.matcher(userName);   
        if(m.find()){
            System.out.println(userName + " is valid UserName");
        }
        else {
            System.out.println(userName + " is not valid UserName");
        }
    }
}
