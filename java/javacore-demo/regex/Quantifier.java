/*
Question :
	1.write a program for java regex quantifer?

Requirement :
	Write a program for Java String Regex quantifer.

Entities :
	Quantifier
	
Job to be Done :
	1.Create class Quantifier and main method.
	2.Create string sentence.
	3.Using pattern class and enter the quantifier patter in compile method.
	4.Using matcher class, check wheather pattern class matches the sentance.
	5.And the using while loop find matches found print the character and index.
*/

package com.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class Quantifier {
     
    public static void main(String[] args) { 
    
        String sentence = "abaabbaaabbbaaaabab";
        Pattern p = Pattern.compile("a*");
        Matcher m = p.matcher(sentence);   
        while(m.find()){
            System.out.println(m.group()+" \tstarts at "+m.start());
        }    
    }
}
