/*
Question :

2. For the following code use the split method() and print in sentance
    String website = "https-www-google-com";
    

Requirement :
	Write a program for Java String Regex Methods.

Entities :
	SplitMethod
	
Job to be Done :
	1.Create class SplitMethod and main method.
	2.Using string website and use split() method, splits the string into N substrings 
	  and returns a sentance.
	3.Print sentance using for each loop.
*/

package com.java.core.regex;

public class SplitMethod {
	
	public static void main(String[] args) {
		 String website = "https-www-google-com";
	    for (String display: website.split("-")) {
	         System.out.print(display+" ");
	    }  
	}
}
