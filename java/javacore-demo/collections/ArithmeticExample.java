/*
 Requirements:
  - Addition,Subtraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface.
  
 Entities:
  - ArithmeticExample
  - Arithmetic(Interface)
  
 Function Declaration:
  - int operation(int a, int b);
  - public static void main(String[] args)
  
 Jobs to be done:
  1.Declare and Initialize the number of type integer a and b.
  2.Add the number a and b ,store it in addition.
  3.Subtract the number a and b ,store it in subtraction.
  4.Multiply the number a and b ,store it in multiplication.
  5.Divide the number a and b ,store it in division.      
*/
package com.java.core.collections;

interface Arithmetic {
	int operation(int a, int b);
}

public class ArithmeticExample {
	
	public static void main(String[] args) {
		
		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(10, 6));

		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(10, 6));

		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(10, 6));

		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 6));
		
	}
}
