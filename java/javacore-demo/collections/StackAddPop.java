/*
 Requirements:
  - add and remove the elements in stack
  
 Entities:
  - StackAddPop
  
 Function Declaration:
  - public static void main(String[] args)
  
 Jobs to be done:
  1.Create a stack list and add 5 elements in this stack
  2.If given the stack name
    2.1)Check the input is null or any special character ,these conditions are not satisfied.
  3.Remove the stack element
  4.Print the stack.   
*/
package com.java.core.collections;

import java.util.*; 

public class StackAddPop { 
	
	public static void main(String[] args) { 

		Stack<String> stack = new Stack<String>(); 
 
		stack.add("Java"); 
		stack.add("is"); 
		stack.add("a"); 
		stack.add("programming"); 
		stack.add("language"); 

		System.out.println("Stack: " + stack); 
		String rem_ele = stack.remove(4); 
		System.out.println("Removed element: " + rem_ele); 
		System.out.println("Final Stack: " + stack); 
		
	} 
	
} 