/*
 Requirement:
  - code for sorting vector list in descending order.
 Entity:
  - SortingVectorExample
 Function Declaration:
  - public static void main(String[] args)  
 Jobs to be done:
  1) Create a Vector object
  2) Add elements to the Vector using add() method
  3) Sort descending order using Collection.reverseOrder()
  4) Display the reverse order elements list.
*/
package com.java.core.collections;

import java.util.Collections;
import java.util.Vector;

public class SortingVector {
	
    public static void main(String[] args) {

        Vector<String> vector = new Vector<String>();
 
        vector.add("Giraffe");
        vector.add("Cat");
        vector.add("Zebra");
        vector.add("Bear");
        vector.add("Shark");
 
        System.out.println("Vector elements before sorting: ");
        for(int i=0; i < vector.size(); i++){
            System.out.println(vector.get(i));
        }

        Collections.sort(vector, Collections.reverseOrder());

        System.out.println("Vector elements after sorting :");
        for(int i=0; i < vector.size(); i++){
            System.out.println(vector.get(i));
        }
        
    } 
    
}