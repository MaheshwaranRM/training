
/*
 Requirements:
  - use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
     methods to store and retrieve elements in ArrayDequeue.
     
 Entities:
  - DequeExample
  
 Function Declaration:
  - public static void main(String[] args)
  
 Jobs to be done:
  1.Create a linkedList 
  2.Use Deque methods 
    2.1)Add an element in a Deque, we can use the add() method, another two method addFirst() and addLast(). 
    2.2)Remove an element in a Deque, we can use the removeFirst(),removeLast().
    2.3)Remove the element at the head of the deque,we can use pollFirst().
    2.4)Remove the element at the tail of the deque,we can use pollLast().
    2.5)Does not remove, the first element of this deque,we can use peekFirst().
    2.6)Does not remove, the Last element of this deque,we can use peekLast().
  3.Display the deque.  
 */
 
package com.java.core.collections;

import java.util.*; 

public class DequeueExample { 
	
	public static void main(String[] args) { 
		
		Deque<String> deque = new LinkedList<String>(); 
		
		deque.add("is");
		deque.add("a");
		deque.add("Programming");
		deque.add("Language"); 	
		deque.add("and");
		deque.add("easy");
		deque.add("to");
		deque.addFirst("Java"); 
		deque.addLast("Understand"); 	
		System.out.println(deque);
		deque.removeFirst(); 
		deque.removeLast(); 
		
		System.out.println("Deque after removing " + "first and last: " + deque); 
		System.out.println("pollLast(): "+deque.pollLast());
		System.out.println("pollFirst(): "+deque.pollFirst());
		System.out.println("peekLast(): "+deque.peekLast());
		System.out.println("peekFirst(): "+deque.peekFirst());
		System.out.println(deque);
	}  
}