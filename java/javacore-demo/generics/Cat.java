/*Generics-Class literals:
1. Write a program to demonstrate generics - class objects as type literals.
2. Write a program to demonstrate generics - for loop, for list, set and map. 
  --------------------------------WBS--------------------------------------
Requirement:
    - Write a program to demonstrate generics - class objects as type literals.
    - Write a program to demonstrate generics - for loop, for list, set and map.

Entities:
    - public class Dog

Function Declaration:
    - public static void main(String[] args)
    - public void sound()
    - public static <T> boolean checkInterface(Class<?> theClass)
    
Jobs to be Done:
    1.Create a class Dog and implenting Animal interafce with single sound method().
    2.Declare a method public static <T> boolean checkInterface(Class<?> theClass) returning theClass.isInterface()
    3.In the sound() method printing "Barking" and declare main method.
    4.Integer class , Dog class and Animal interface 
    4.Declare integer class and printing boolean , getClass() method for getting class and getName() method for getting type
    5.Using TryCatch exception handling Checking class , getClass and getName presenr or not
---------------------------------------Program--------------------------------
*/

package com.java.core.generics;

interface Animal {
	public void sound();
}

public class Cat implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         

        boolean boolean2 = checkInterface(Cat.class);
        System.out.println(boolean2);                   
        System.out.println(Cat.class.getClass());       
        System.out.println(Cat.class.getName());        

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   
        System.out.println(Animal.class.getClass());    
        System.out.println(Animal.class.getName());     

        try {
            Class<?> errClass = Class.forName("Cat");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}
/*
---------Output------------
false
class java.lang.Class
int
false
class java.lang.Class
Cat
true
class java.lang.Class
Animal
class java.lang.Class
Cat


*/