/*Requirements:
   Write a java program of performing two tasks by two threads that implements Runnable Interface.
   
Entities:
   Multitasking
   
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
    1.Create two classes called sample 1 and sample 2 which implements runnable interface.
    2.Create a method run() in both classes.
    3.In main class create a threads t1 and t2 and pass the objects of both class.  
    4.Start the thread process.
*/

package com.java.core.concurrency;

class Sample1 implements Runnable{
	 public void run(){
	   System.out.println("Start task one");
	 }
	}
class Sample2 implements Runnable{
	 public void run(){
	   System.out.println("Start task two");
	 }
	}
public class Multitasking{
	 public static void main(String[] args){
	  Thread t1=new Thread(new Sample1());
	  Thread t2=new Thread(new Sample2());
          t1.start();
	  t2.start();
	 }
	}