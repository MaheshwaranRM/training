/*
Question :
	4.Find out the IP Address and host name of your local computer.
 
1.Requirement:
     Find out the IP Address and host name of your local computer.
     
2.Entities:
       AddressHostName
      
3.JobsToBeDone:
  	1)Try block
  	     1.1)Create InetAddress class to use get method.
  	     1.2)Invoke a method to get ipaddress and host name.
  	2).Print the values.
  
PseudoCode:

	public class AddressHostName {
	   public static void main(String[] args) {
	      try{
	         IP address and host name using methods      
	         }
	      catch (UnknownHostException e){
	        print error message   
	        }
	   }
	}

*/

package com.java.core.networking;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AddressHostName {
	public static void main(String[] args) {
		try {
			InetAddress my_address = InetAddress.getLocalHost();
			System.out.println("LocalHost is : " + my_address);
			System.out.println("The IP address is : " + my_address.getHostAddress());
			System.out.println("The host name is : " + my_address.getHostName());
			// ip address of a website
			my_address = InetAddress.getByName("google.com.sa");
			System.out.println("Google inetaddress is : " + my_address);
		} catch (UnknownHostException e) {
			System.out.println("Couldn't find the local address.");
		}
	}
}
