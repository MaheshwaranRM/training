/*
Requirements: 
    To write data to multiple files together using bytearray and outputstream.
  
Entity: 
    MultipleFileWriteDemo
  
Method Signature: 
    public static void main(String[] args);
  
Jobs to be done: 
    1. Create OutputStreams for files space.txt and star.txt.
  	2. Initialise the string value to be written to string.
  	3. Convert the string to byte array.
  	4. Write the string to file using OutputStream
    
PseudoCode:

public class MultipleFileWriteDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("d:\\series\\space.txt");
		OutputStream outputStream1 = new FileOutputStream("d:\\series\\star.txt");
		String string = "Write " + "Using " + "OutputStream"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		
		outputStream.close();
		outputStream1.close();
	}
}


 */
 
package com.java.core.iostreams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MultipleFileWriteDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("d:\\series\\space.txt");
		OutputStream outputStream1 = new FileOutputStream("d:\\series\\star.txt");
		String string = "Write " + "Using " + "OutputStream"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		
		outputStream.close();
		outputStream1.close();
	}
}