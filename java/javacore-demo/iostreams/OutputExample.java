/*
Requirements:
   program to write primitive data types to file using by dataoutputstream.

Entities:
   OutputExample
   
Function declaration:
   public static void main(String[] args) throws IOException
   
Jobs to be done:
    1.create class named as OutputExample
    2.In main method,throws IOException is used
        2.1 Set the path of the file and create odjects as file and data
        2.2 Set the data object in type integer value
    3.Use close() method,to close the file.
    4.Print the output.

Pseudo Code:

public class OutputExample {

	public static void main(String[] args) throws IOException {  
		
        FileOutputStream file = new FileOutputStream("C:\\1dev\\java\\space.txt");  
        DataOutputStream data = new DataOutputStream(file);
        
        data.writeInt(65);  
        data.flush();  
        data.close();  
        System.out.println("Success...");  
    
	}  

}  
*/
package com.java.core.iostreams;

import java.io.*; 

public class OutputExample {

	public static void main(String[] args) throws IOException {  
		
        FileOutputStream file = new FileOutputStream("C:\\1dev\\java\\space.txt");  
        DataOutputStream data = new DataOutputStream(file);
        
        data.writeInt(65);  
        data.flush();  
        data.close();  
        System.out.println("Success...");  
    
	}  

}  