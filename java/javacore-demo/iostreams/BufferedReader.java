/*
Requirements:
    To read any text file using bufferedReader and print the content of the file.

Entities:
    BufferedReader

FunctionDeclaration:
    public static void main(String[] args)

Jobs to be done:
    1.create a class called BufferedReader
    2.In main method,try and catch block is used.
        2.1 In First,Set the path to read the file and declare the variable as i
        2.2 In while condition,reading the file until it gets false and print the result
    3.After read the file,Use close()method is used to close the file.

Pseudo code:

public class BufferedReader {

	public static void main(String[] args) {
          // create try and catch block
		 try{    
		    FileInputStream fin=new FileInputStream("C:\\data\\byteoutput1.txt");    
		    BufferedInputStream bin=new BufferedInputStream(fin);  //set the path to read the file  
		    int i;    
		    while((i=bin.read())!=-1){    
		     System.out.print((char)i);    
		    }    
		    bin.close();    
		    fin.close();    
		  }catch(Exception e)
		 {
			  System.out.println(e);
		 }      
	}

}

*/

package com.java.core.iostreams;

import java.io.*; 

public class BufferedReader {

	public static void main(String[] args) {
		try {    
		    FileInputStream fin=new FileInputStream("C:\\1dev\\java\\space.txt");    
		    BufferedInputStream bin=new BufferedInputStream(fin);    
		    int i;    
		    while((i=bin.read())!=-1){    
		       System.out.print((char)i);    
		  }    
		    bin.close();    
		    fin.close();    
		  } catch(Exception e) {
			   System.out.println(e);
		  }      
	}

}