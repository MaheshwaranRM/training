/*
Requirements: 
    To write a program for ByteArrayInputStream class to read byte array as input stream.
  
Entities: 
    ByteArrayAsInputStream
  
Method Signature: 
    public static void main(String[] args);
  
Jobs to be done: 
    1. Create a byte array and store values to it.
  	2. Create a ByteArrayInputStream by passing byteArray as argument.
 	3. While the byte array is not empty
 		3.1) Convert the int to char and print it.
        
PseudoCode:
 
public class ByteArrayAsInputStreamDemo {
	
	public static void main(String[] args) throws IOException {  
		
	    byte[] byteArray = { 35, 36, 37, 38 };  
	    
	    ByteArrayInputStream byt = new ByteArrayInputStream(byteArray);  
	    int k = 0;  
	    while ((k = byt.read()) != -1) {    
	    	char ch = (char) k;  
	    	System.out.println("ASCII value of Character is:" + k + "; Special character is: " + ch);  
	    }  
	}  
}  
 */
 
package com.java.core.iostreams;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayAsInputStreamDemo {
	
	public static void main(String[] args) throws IOException {  
		
	    byte[] byteArray = { 35, 36, 37, 38 };  
	    
	    ByteArrayInputStream byt = new ByteArrayInputStream(byteArray);  
	    int k = 0;  
	    while ((k = byt.read()) != -1) {   
	    	
	    	char ch = (char) k;  
	    	System.out.println("ASCII value of Character is:" + k + "; Special character is: " + ch);  
	    }  
	}  
}