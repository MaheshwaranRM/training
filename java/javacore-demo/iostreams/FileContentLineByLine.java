/*
Requirements: 
    To read the file contents  line by line in streams with example.
  
Entities: 
    FileContentLineByLine
  
Method Signature: 
    public static void main(String[] args);
  
Jobs to be done: 
    1. Read the file content in lines as stream.
  	2. Fetch each line one at a time and print it.
  	3. Check if exception raises in accessing line by line.
  		3.1) Handle the exception.
        
PseudoCode:
  
public class FileContentLineByLine {
	
	public static void main(String[] args) throws IOException {
		
		//Get the path of file
		try (Access file line by line) {

			stream.forEach(System.out.println);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

 */
 
package com.java.core.iostreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileContentLineByLine {
	
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get("d:\\series\\space.txt");
		try (Stream<String> stream = Files.lines(filePath)) {

			stream.forEach(System.out::println);

		} catch (IOException iOException) {
			iOException.printStackTrace();
		}
	}
}