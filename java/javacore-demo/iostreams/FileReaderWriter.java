/*
Requirements: 
    To read data from a particular file using FileReader and writes it to another, using FileWriter.
  
Entities: 
    FileReaderWriter
  
Method Signature: 
    public static void main(String[] args);
  
Jobs to be done: 
    1. Create a FileWriter to save the destination file path.
  	2. Create a FileReader and initialise to null for the source file path.
  	3. Check if exception raises in the creating file reader and writer.
  		3.1) Handle the exception.
  	4. While the file has not reached end.
  		4.1) Read each characters and write it to destination file.
  	5. Close the FileWriter and FileReader.
                    
PseudoCode:

public class FileReaderWriter {
	
	public static void main(String[] args) throws IOException 
    {  
        int ch; 

    	FileWriter fw = null;
        FileReader fr = null; 
        try { 
            fr = new FileReader("d:\\series\\space.txt"); 
            fw = new FileWriter("d:\\series\\star.txt"); 
            
        } catch (FileNotFoundException fe) { 
            System.out.println("File not found"); 
        } 
  
        while ((ch = fr.read()) != -1)  {
        
        	fw.write(ch); 
    	}
        fr.close(); 
        fw.close();
    } 
}
*/
package com.java.core.iostreams;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriter {
	
	public static void main(String[] args) throws IOException 
    {  
        int ch; 

    	FileWriter fw = null;
        FileReader fr = null; 
        try { 
            fr = new FileReader("d:\\series\\space.txt"); 
            fw = new FileWriter("d:\\series\\star.txt"); 
            
        } catch (FileNotFoundException fe) { 
            System.out.println("File not found"); 
        } 
  
        while ((ch = fr.read()) != -1)  {
        
        	fw.write(ch); 
    	}
        fr.close(); 
        fw.close();
    } 
}
 