package com.java.core.lambdaexpression;

interface Printer {
    public void printString(String string);
}

class Reference {
    
    public void display(String string){
        System.out.println(string);
    }
    public static void main(String[] args ) {
        
        //Creating an object for class InstanceReference
    	Reference object = new Reference();
        
                        //ObjectOfClass::instanceMethod
        Printer printer = object::display;
        
        //Invoking interface method
        printer.printString("Instance Method Reference");
    }
}