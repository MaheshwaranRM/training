/*  Write a program to print the volume of a Rectangle using lambda expression.

  ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Program to print the volume of a Rectangle using lambda expression.
2.Entities
   - VolumeRectangle
   - PrismValues (Interface)
3.Function Declaration
   - int Values(int base,int length,int height);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as VolumeRectangle with interface as PrismValues.
   2.Inside the declaring the single method with integer parameters.
   3.In the class main creating interface object and assigning with passing prism values returning prism value.
   4.Print statement invoking the interface single method with multiple integer value and finally return value using assigned
interface object lambda expression.

-----------------------------------------------Program--------------------------------------------------------------------------------*/

package com.java.core.lambdaexpression;

interface PrismValues {
	int Values(int base,int length,int height);
}


public class VolumeRectangle {

	public static void main(String[] args) {
		PrismValues prismValues = (base,length,height) -> base * length * height;
		System.out.println(prismValues.Values(5,8,16));

	}

}
