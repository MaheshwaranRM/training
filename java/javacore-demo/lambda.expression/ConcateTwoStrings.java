/*Write a Lambda expression program with a single method interface to concatenate two strings
  
----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Lambda expression Program with a single method interface to concatenate two strings
2.Entities
   - ConcateTwoStrings
   - SingleMethodInterface (Interface)
3.Function Declaration
   - String string(String string1, String string2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as ConcateTwoStrings with interface as SingleMethodInterface
   2.Inside the declaring the single method with two string parameters
   3.In the class main creating interface object and assigning the lambda expression to return two strings concatenate 
   4.Print statement invoking the interface single method with string values and finally return the value using assigned
interface object lambda expression 


-----------------------------------------------Program--------------------------------------------------------------------------------

Lambda expression program with a single method interface to concatenate two strings  */

package com.java.core.lambdaexpression;
interface SingleMethodInterface {
	String string(String string1, String string2);
}

public class ConcateTwoStrings {

	public static void main(String[] args) {
		//Assigning the lambda expression to return two strings concatenate
		SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
		//invoking the interface single method with string values 
		System.out.println(singleInterface.string("Single ", "Method Interface"));
	}

}
