Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }

  ----------------------------------------Word Breakdown Structure(WBS)-------------------------------------------------------------
1.Requirements
   - Program to print the volume of a Rectangle using lambda expression.
2.Function Declaration
   - int getValue()
3.Jobs to be done
   1.Converting given method into lambda expression.

-----------------------------------------------Answer--------------------------------------------------------------------------------

  Lambda Expression:
     
     getValue = () -> 5 
     
     Assigning the returning value to the variable get value using lambda expression