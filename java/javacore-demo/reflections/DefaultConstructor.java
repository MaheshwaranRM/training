/*
 Requirements:
    Private Constructor with main class. 
   
 Entities:
    No entities
    
 Function Declaration:
    public static void main(String[] args)
    
 Jobs to be done:
    1. Create an example for a Default Constructor.
    2. Display the output.
*/


class DefaultConstructor {

    int a;
    boolean b;

    public static void main(String[] args) {

        DefaultConstructor obj = new DefaultConstructor();// A default constructor is called

        System.out.println("a = " + obj.a);
        System.out.println("b = " + obj.b);
    }
}