/*
 Requirements:
    Private Constructor with main class. 
    
 Entity:
    private PrivateConstructor
    
 Function Declaration:
    public static void main(String[] args)
    
 Jobs to be done:
   1. Create a private constructor.
   2. Create a public static method.
   3. Create an instance of PrivateConstructor class.
   4. Create main class.
   5. Call the instance method.
   4. Display the output.
*/

class PrivateConstructor {

    private PrivateConstructor () {
        System.out.println("This is a private constructor.");
    }

    public static void instanceMethod() {
        PrivateConstructor obj = new PrivateConstructor();
    }
}

class Main {

    public static void main(String[] args) {
        PrivateConstructor.instanceMethod();// call the instanceMethod()
    }
}