/*
 Requirements:
    Private Constructor with main class. 
    
 Entities:
    private ParameterizedConstructor
    
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a Parameterized Constructor.
   2. Display the output.
*/

class ParameterizedConstructor {

    int wheels;
    
    private ParameterizedConstructor(int wheels) {// constructor accepting single value
        this.wheels = wheels;
        System.out.println(wheels + " wheeler vehicle created.");
    }

    public static void main(String[] args) {

        ParameterizedConstructor p1 = new ParameterizedConstructor(2);// calling the constructor by passing single value
        ParameterizedConstructor p2 = new ParameterizedConstructor(3);
        ParameterizedConstructor p3 = new ParameterizedConstructor(4);
    }
}