/*
 Requirements:
    Private Constructor with main class. 
   
 Entities:
    Private Main()
   
 Function Declaration:
    public static void main(String[] args)
   
 Jobs to be done:
   1. Create an example for a No-Arg Constructor.
   2. Display the output.
*/

class NoArgConstructor {

    int i;
    private NoArgConstructor(){// constructor with no parameter
        i = 5;
        System.out.println("Object created and i = " + i);
    }

    public static void main(String[] args) {

        Main NoArgConstructor = new NoArgConstructor();// calling the constructor without any parameter
   }
}