/*
Requirements:
     - Write a program which implement multithread using 
       sleep(), setPriority(), getPriorty(), Name(), getId() methods.
       
Entities:
     - ThreadDemo
     
Function Declaration:
     - public void run()
     - public static void main(String[]args)
     
Jobs to be done:
     1.Create a method run() and thread instances 
     2.Implement the Multithread using methods of
       2.1)If the getPriority() method print the default value 5.
       2.2)If the setPriority() method print the set value of 2
       2.3)If the getName() method print the name of thread
       2.4)If the getId() method print the id of thread.
     3.Print the values.
     
Pseudocode:

public class ThreadDemo {

   public void run() {
     
   }
   
   public static void main(String[]args) {
      
       ThreadDemo t1 = new ThreadDemo();
       System.out.println("t1 thread priority : " + t1.getPriority()); 
       t1.setPriority(2); 
       System.out.print(Thread.currentThread().getName());                  
       System.out.println("Id of t1: " + t1.getId());
       
   }
 
}       
*/
package com.java.core.multithreading;
 
import java.lang.*; 

public class ThreadDemo extends Thread { 
	
	public void run() { 
		System.out.println("Inside run method");    
	}    

	public static void main(String[]args) { 
		
		ThreadDemo t1 = new ThreadDemo(); 
		ThreadDemo t2 = new ThreadDemo(); 
		ThreadDemo t3 = new ThreadDemo(); 

		System.out.println("t1 thread priority : " + t1.getPriority()); // Default 5 
		System.out.println("t2 thread priority : " + t2.getPriority()); // Default 5 
		System.out.println("t3 thread priority : " + t3.getPriority()); // Default 5 

		t1.setPriority(2); 
		t2.setPriority(5); 
		t3.setPriority(8); 

		System.out.println("t1 thread priority : " + t1.getPriority()); //2 
		System.out.println("t2 thread priority : " + t2.getPriority()); //5 
		System.out.println("t3 thread priority : " + t3.getPriority());//8 

		// Main thread 
		System.out.print(Thread.currentThread().getName()); 
		System.out.println("Main thread priority : " + Thread.currentThread().getPriority()); 
		
		// Main thread priority is set to 10 
		Thread.currentThread().setPriority(10); 
		System.out.println("Main thread priority : " + Thread.currentThread().getPriority());

		System.out.println("Id of t1: " + t1.getId());
		System.out.println("Id of t2: " + t2.getId());
		System.out.println("Id of t3: " + t3.getId());
		
	} 
	
} 
