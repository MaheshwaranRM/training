package com.java.core.serialization;

import java.io.Serializable;

public class Student implements Serializable {

	
	private String name;
	private int studentId;
    private String address;
    private int phoneNumber;
    
    @Override
	public String toString(){
		return "\nName : "+name + " StudentId : "+studentId + " Address : " + address + " PhoneNumber : " + phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
       
    
	
}