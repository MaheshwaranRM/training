/*
Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File and print the content.


1.Requirements:
     Program to store the input in the File and serialize it, and again de serialize the File and print the 
content.

2.Entities:
      SerializationDemo

3.Jobs to be done:
    1.Get an input from the user for name, stuentid, address, phonenumber.
    2.Try block
      2.1)Set the path to store ser file and store it in fileout.
      2.2)Create an object for OutputStream name out for fileOut.
      2.3)Write in using writeObject method and close ObjectOutputStream writeFile object.
    3.Catch IOException invoke printStackTrace method. 

Psudocode:

public class SerializationDemo {

		public static Object deserialize(String fileName) throws IOException,
				ClassNotFoundException {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();
			ois.close();
			return obj;
		}

		public static void serialize(Object obj, String fileName)
				throws IOException {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			fos.close();
		}
		public static void main(String[] args) {
			String fileName="student.ser";
			Student stud = new Student();
			stud.setName("Praveen");
			stud.setStudentId(100);
			stud.setAddress("Coimbatore");
			stud.setPhoneNumber(123456789);
			
			try {
				SerializationDemo.serialize(stud, fileName);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			Student studNew = null;
			try {
				studNew = (Student) SerializationDemo.deserialize(fileName);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			
			System.out.println("Serialized : "+stud);
			System.out.println("Deserialized : "+studNew);
		}
}

*/

package com.java.core.serialization;

import java.io.*;
public class SerializationDemo {
	// deserialize to Object from given file
		public static Object deserialize(String fileName) throws IOException,
				ClassNotFoundException {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();
			ois.close();
			return obj;
		}

		// serialize the given object and save it to file
		public static void serialize(Object obj, String fileName)
				throws IOException {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			fos.close();
		}
		public static void main(String[] args) {
			String fileName="student.ser";
			Student stud = new Student();
			stud.setName("Mahesh");
			stud.setStudentId(75);
			stud.setAddress("Coimbatore");
			stud.setPhoneNumber(123456789);
			
			//serialize to file
			try {
				SerializationDemo.serialize(stud, fileName);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			Student studNew = null;
			try {
				studNew = (Student) SerializationDemo.deserialize(fileName);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			
			System.out.println("Serialized : "+stud);
			System.out.println("Deserialized : "+studNew);
		}
}
