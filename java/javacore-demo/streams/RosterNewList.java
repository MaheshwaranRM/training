/*Requirements:
    To  - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
        
Entities:
  RosterNewList
  
Function Declaration : 
    public static void main(String[] args);
    
Jobs To be Done: 
                    1. Create a list of type person and add the list values to it. 
 		    2. PCreate a second list and add all values.
                    3. Addd all the vaues to the same list.
                    4. Print the size of the list.
                    5. Perform all the elements from the list.
                    
PseudoCode:
public class RosterNewList {

	public static void main(String[] args) {
		
         List<Person> newRoster = new ArrayList<>();
         newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE, "john@example.com"));
         newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15), Person.Sex.FEMALE, "jade@example.com"));
         newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13), Person.Sex.MALE, "donald@example.com"));
         newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com"));
         
         List<Person> list = Person.createRoster();
         list.addAll(newRoster);
         
         list.forEach(y -> System.out.println(y.getName() + " " + y.getEmailAddress() + " " + y.getAge()));
         System.out.println(list.size());
         
         list.clear();
         System.out.println(list);
	}
}



 */
package com.java.core.streams;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class RosterNewList {

	public static void main(String[] args) {
		
		 List<Person> newRoster = new ArrayList<>();
         newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE, "john@example.com"));
         newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15), Person.Sex.FEMALE, "jade@example.com"));
         newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13), Person.Sex.MALE, "donald@example.com"));
         newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com"));
         
         List<Person> list = Person.createRoster();
         list.addAll(newRoster);
         
         list.forEach(y -> System.out.println(y.getName() + " " + y.getEmailAddress() + " " + y.getAge()));
         System.out.println(list.size());
         
         list.clear();
         System.out.println(list);
	}
}
