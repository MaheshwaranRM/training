/*
Requirements: 
    To filter the Person, who are male and age greater than 21
    
Entities:
  PersonAge
  
Function Declaration : 
  public static void main(String[] args);
  
Jobs To be Done: 
1. Create a list of type person and add the list values to it. 
2. Get the ages of the peopleand filter the names whose age is greater than 21
3.Find the names of the people.

PseudoCode:
public class PersonAge {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Find the names of the people whose age is greater than 21.
		stream.forEach(y -> System.out.println(y.getName()));
	}
}

 */
package com.java.core.streams;

import java.util.List;
import java.util.stream.Stream;

public class PersonAge {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Stream<Person> stream = list.stream().filter(x -> (x.getGender() == Person.Sex.MALE) && (x.getAge() > 21));
		stream.forEach(y -> System.out.println(y.getName()));
	}
}
