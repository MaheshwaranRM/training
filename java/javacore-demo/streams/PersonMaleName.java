/*Requirements:
          To  to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons
        
Entities:
     PersonMaleName
     
Function Declaration : 
   public static void main(String[] args);

Jobs To be Done:
    1.Create a list of type person and add the list values to it. 
    2.Filter the details for the male gender.
    3.Find the first person, last person and random person and print it.

 PseudoCode:
 public class PersonMaleName {
	
	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Find names with gender Male
		System.out.println(result.get(0).getName());
		System.out.println(result.get(result.size()-1).getName());
		System.out.println(result.get(1).getName());
	}

}

 */
package com.java.core.streams;

import java.util.List;
import java.util.stream.Collectors;

public class PersonMaleName {

	public static void main(String[] args) {
			
			List<Person> list = Person.createRoster();
			List<Person> result = list.stream().filter(x -> (x.getGender() == Person.Sex.MALE)).collect(Collectors.toList());
			System.out.println(result.get(0).getName());
			System.out.println(result.get(result.size()-1).getName());
			System.out.println(result.get(1).getName());
			
	}
}


