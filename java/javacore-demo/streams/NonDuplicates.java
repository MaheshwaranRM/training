/*Requirements:
    To List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
            - Get the non-duplicate values from the above list using java.util.Stream API
            
Entities:
    NonDuplicates
    
Function Declaration : 
    public static void main(String[] args);
    
Jobs To be Done: 
1. Declare array and store values into it.
2. convert the array into list.
3.Find the non duplicate elements in the list.
4. Find the average of the age and print it.

 PseudoCode:
public class NonDuplicates {
 
	public static void main(String[] args) {
		
		Integer array[] = new Integer[] {1, 6, 10, 1, 25, 78, 10, 25} ; 
		
		List<Integer> randomNumbers = Arrays.asList(array);
		find the non duplicate values using java.util.Stream
		System.out.println(list);
	}
}


 */
package com.java.core.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NonDuplicates {
 
	public static void main(String[] args) {
		
		Integer array[] = new Integer[] {1, 6, 10, 1, 25, 78, 10, 25} ; 
		
		List<Integer> randomNumbers = Arrays.asList(array);
		List<Integer> withoutDupes = randomNumbers.stream().distinct().collect(Collectors.toList());
		System.out.println(withoutDupes);
	}
}
