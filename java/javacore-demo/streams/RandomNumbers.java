/*Requirements:
          List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API
Entities: 
   RandomNumbers
   
Function Declaration : 
  public static void main(String[] args);

Jobs To be Done: 1. Create a list of type person and add the list values to it. 
 		 2. Find the sum of all numbers in the list and print it.
  		 3. Find the max and min element in the list and print it.
         
 PseudoCode:
 
public class RandomNumbers {

	public static void main(String[] args) {
	
		Integer array[] = new Integer[] {1, 6, 10, 25, 78} ; 
		
		List<Integer> randomNumbers = Arrays.asList(array);
		Find sum of elements using stream API
		System.out.println(sum);
		
		int max = IntStream.range(0, array.length).map(i -> array[i]).max().getAsInt();
		int min = IntStream.range(0, array.length).map(i -> array[i]).min().getAsInt();
		System.out.println("Max is:" +max);
		System.out.println("Min is: "+min);
	}
}

*/
package com.java.core.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class RandomNumbers {

	public static void main(String[] args) {
	
		Integer array[] = new Integer[] {1, 6, 10, 25, 78} ; 
		
		List<Integer> randomNumbers = Arrays.asList(array);
		int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
		System.out.println(sum);
		
		int max = IntStream.range(0, array.length).map(i -> array[i]).max().getAsInt();
		int min = IntStream.range(0, array.length).map(i -> array[i]).min().getAsInt();
		System.out.println("Max is:" +max);
		System.out.println("Min is: "+min);
	}
}
