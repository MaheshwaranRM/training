/*Requirements:
    To  sort the roster list based on the person's age in descending order using comparator
    
Entities: 
   SortAgeList
   
Function Declaration : 
   public static void main(String[] args);
   
Jobs To be Done: 
     1. Create a list of type person and add the list values to it. 
     2. Print the details of people in the descending order.
     
PseudoCode:

public class SortAgeList {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		compare the age and sort it using comparator and print it.
	}
}

 */
package com.java.core.streams;

import java.util.Comparator;
import java.util.List;

public class SortAgeList {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		list.stream().sorted(Comparator.comparingLong(person -> person.getAge())).forEach(person -> System.out.println(person.getName() +" " + person.getAge()));
		}
}
