/*Requirements:
        To Remove the only person who are in the newRoster from the roster list.
         - Remove the following person from the roster List:
           new Person("Bob",IsoChronology.INSTANCE.date(2000, 9, 12),Person.Sex.MALE, "bob@example.com"));
           
Entities:
   PersonCheck
   
Function Declaration : 
   public static void main(String[] args);

Jobs To be Done: 
1. Create a list of type person and add the list values to it. 
2. Create a new list and add given values.
3. Find and remove the values in the second list from the first list.
4. Remove the given details from the list. 

PseudoCode:

public class PersonAge {

	public static void main(String[] args) {
		
		boolean p1 = Person.createRoster().contains(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),Person.Sex.MALE, "bob@example.com"));
		System.out.println(p1);
		
		List<Person> list = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
		newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20),  Person.Sex.MALE,  "john@example.com"));
        newRoster.add( new Person( "Jade", IsoChronology.INSTANCE.date(1990, 7, 15),  Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add( new Person( "Donald", IsoChronology.INSTANCE.date(1991, 8, 13), Person.Sex.MALE, "donald@example.com"));
        newRoster.add( new Person( "Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com"));
        Person person = new Person( "Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com");
        newRoster.remove(person);
        list.retainAll(newRoster);
        newRoster.removeIf(x -> x.equals(person));

        newRoster.forEach(t -> System.out.println(t.getName() + " " + t.getEmailAddress()));
	}
}

 */
package com.java.core.streams;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class PersonCheck {

	public static void main(String[] args) {
		
		boolean p1 = Person.createRoster().contains(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),Person.Sex.MALE, "bob@example.com"));
		System.out.println(p1);
		
		List<Person> list = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
		newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20),  Person.Sex.MALE,  "john@example.com"));
        newRoster.add( new Person( "Jade", IsoChronology.INSTANCE.date(1990, 7, 15),  Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add( new Person( "Donald", IsoChronology.INSTANCE.date(1991, 8, 13), Person.Sex.MALE, "donald@example.com"));
        newRoster.add( new Person( "Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com"));
        Person person = new Person( "Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE, "bob@example.com");
        newRoster.remove(person);
        list.retainAll(newRoster);
        newRoster.removeIf(x -> x.equals(person));

        newRoster.forEach(t -> System.out.println(t.getName() + " " + t.getEmailAddress()));
        
	}
}
