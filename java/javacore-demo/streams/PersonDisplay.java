/*Requirements:
    To Print all the persons in the roster using java.util.Stream<T>#forEach 
    
Entities:
  PersonDisplay
  
Function Declaration : 
   public static void main(String[] args);
   
Jobs To be Done: 
1. Create a list of type person and add the list values to it. 
2. Convert the list to stream.
3.Print the names of people.

PseudoCode:
public class PersonAge {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Stream<Person> result = list.stream();
		stream.forEach(y -> System.out.println(y.getName()));
	}
}

 */
package com.java.core.streams;

import java.util.List;
import java.util.stream.Stream;

public class PersonDisplay {
	 
	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Stream<Person> result = list.stream();
		result.forEach(y -> System.out.println(y.getName()));
	}

}
