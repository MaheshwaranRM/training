/*6. Write a program to find the average age of all the Person in the person List
Requirements:

    Write a program to find the average age of all the Person in the person List

Entities:

-AverageAge

Method signature:

public static void main(String[] args)

Jobs to be done:

1) Call the createRoster static method using the class, the method returns a list
2) Store the returned list to a new list
3) Stream method to get sequential elements in List
4) average method used to find the average of elements.

Pseudo Code:

public class AverageAge {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.

        double age = list.stream().mapToDouble(person -> person.getAge()).average();
        //Stream method to get sequential elements in List
        //mapToDouble() maps to double for every sequential elements
        System.out.print(age);
    }
}
*/

package com.java.core.streams;

import java.util.List;

public class AverageAge {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        double age = personList.stream()
                .mapToDouble(Person::getAge).average().getAsDouble();
        // stream method used to pipeline various methods
        // with the help of getAge gets age of all objects
        //average() method calculates the average for sequential elements
        //getAsDouble() method converts and return Optional double to double 
        System.out.println("Average age of all persons: " + age);
    }

}
