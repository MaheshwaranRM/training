/*
Requirements: 
    To Iterate the roster list in Persons class and and print the person without using forLoop/Stream  
    
Entities:
   RosterDetails
   
Function Declaration : 
   public static void main(String[] args);
   
Jobs To be Done: 
   1. Create a list of type person and add the list values to it. 
   2. Print the list using forEach.
   
PseudoCode:
public class RosterDetails {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		list.forEach(y -> System.out.println(y.getName() + " " + y.getEmailAddress() + " " + y.getAge()));
	}
}


 */
package com.java.core.streams;

import java.util.List;

public class RosterDetails {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		list.forEach(y -> System.out.println(y.getName() + " " + y.getEmailAddress() + " " + y.getAge()));
	}
}
