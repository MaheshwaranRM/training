/*
Requirements:
   - Demonstrate the catching multiple exception with example. 
   
Entities:
   - MultipleExceptionExample
     
Function Declaration:
   - public static void main(String[] args)
   
Jobs to be done:
    1. Declare and Initialize the array value in try block.
    2. Execute the try block.
    3. When an exception occurs in try block ,then go to the catch block.
    4. Check the exception, which are known to compile time.
    5. If two or more exception in this catch block ,then it's separated by pipe character |.
    6. Display the message.

*/

/*
Answer: 
  i) In Java 7 it was made possible to catch multiple different exceptions in the same catch block. 
 ii) This is also known as multi catch.
iii) The two exception class names in the first catch block are separated by the pipe character |. 
 iv) The pipe character between exception class names are how you declare multiple exceptions to be caught by 
the same catch clause.

*/

package com.java.core.exceptionhandling;

public class MultipleExceptionExample {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[10];    
            array[10] = 30/0;    
        } catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) { 
            System.out.println(e.getMessage());  
        }
    }    
}