//MultipleCatchBlock:
/*
 Requirements:
    MultipleCatchBlock with example. 
   
 Entities:
    MultipleCatchBlock  
   
 Function Declaration:
    public static void main(String[] args)
   
 Jobs to be done:
    1. Declare and Initialize the array value in try block.
    2. Execute the try block.
    3. When an exception occurs in try block ,then go to the specific catch block.
    4. Display the output.
  
*/

package exceptionhandling;

public class MultipleCatchBlock {  
	  
	   public static void main(String[] args){
	      try{
	         int arr[]=new int[7];
	         arr[4] = 30/0;
	         System.out.println("Last Statement of try block");
	      }
	      catch(ArithmeticException e){
	         System.out.println("You should not divide a number by zero");
	      }
	      catch(ArrayIndexOutOfBoundsException e){
	         System.out.println("Accessing array elements outside of the limit");
	      }
	      catch(Exception e){
	         System.out.println("Some Other Exception");
	      }
	      System.out.println("Out of the try-catch block");
	   }
}

/*
Output:

You should not divide a number by zero
Out of the try-catch block
Explanation:
In the above example,the first catch block got executed because the code we have written in try block throws ArithmeticException
(because we divided the number by zero).

It is clear that when an exception occurs, the specific catch block(that declares that exception)executes.
*/

/*
b)Reason:
No,it is not possible to have more than one try block,each try block must be followed by catch or finally.
We cannot have multiple try blocks with a single catch block.
Still if we try to have single catch block for multiple try blocks a compile time error is generated.
*/
