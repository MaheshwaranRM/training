/*
Requirement:
   Handle and give the reason for the exception in the following code: 
        PROGRAM:
             public class Exception {  
             public static void main(String[] args)
             {
  	       int arr[] ={1,2,3,4,5};
	      System.out.println(arr[7]);
            }
          }
  Display the output.
  
Entities:
 Exception.
 
Functon declaration:
 public static void main(String[] args)
 
Jobs to be done:
  Have to handle the unchecked exception using try-catch block.
    1.Declare and Initialize the array value in try block.
    2. Execute the try block.
    3. When an exception occurs in try block ,then go to the catch block.
    4. Check the exception, which are known to run time.
    5.Display the output.
   
*/

package com.java.core.exceptionhandling;

public class Exception {  
	   public static void main(String[] args) {
		try{
		   int arr[] ={1,2,3,4,5};
		   System.out.println(arr[7]);
		}
	        catch(ArrayIndexOutOfBoundsException e){
		   System.out.println("The specified index does not exist " +"in array");
		}
	   }
}
	


/*Output:
	   The specified index does not exist in array

Reason: 
	Here Array has only five elements but we are trying to display the value of 8th element.
	It should throw ArrayIndexOutOfBoundsException which is a unchecked exception.
*/