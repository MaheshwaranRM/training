/*
 Requirements:
    - 5.demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) .
        If possible try remaining methods.
        
 Entities:
    - HashMapDemo
    
 Function Declaration:
    - public static void main(String[] args)
    
 Jobs to be done:
    1.Create a HashMap with type integer and String.  
    2.Check the methods    
      2.1)Add elements in hash object using put() method.
      2.2)Remove the element using remove() method
      2.3)Replace the another value using replace() method.
    3.Check the value is contained
      3.1)Print boolean value true or false using containsValue() method.
    4.Display the hash.        
 */
package com.java.core.mapsets;

import java.util.*;
import java.util.HashMap;

public class HashMapDemo {

	public static void main(String[] args) {
		
		HashMap<Integer, String> hash = new HashMap<Integer, String>(); 

		hash.put(10, "Java"); 
		hash.put(15, "Python"); 
		hash.put(20, "Javascript"); 
		hash.put(25, "Php"); 
		hash.put(30, "Ruby"); 

		System.out.println(hash); 

		// Removing the existing key mapping 
		String returned_value = (String)hash.remove(20); 

		// Verifying the returned value 
		System.out.println("Returned value is: "+ returned_value); 

		// Display in the new map 
		System.out.println("New map is: "+ hash);  
		
		//Contains Value
		System.out.println(hash.containsValue("Php"));
		
		//replace the value
		System.out.println(hash.replace(25, "OOPS"));
		
		//Display the values
		System.out.println(hash.values());
		
		//Display the keys
		System.out.println(hash.keySet());

	}

}
