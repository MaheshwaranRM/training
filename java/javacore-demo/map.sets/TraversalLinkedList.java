/*
 Requirements:
  - 2.demonstrate program explaining basic add and traversal operation of linked hash set
  
 Entities:
  - TraversalLinkedList
  
 Function Declaration:
  - public static void main(String[] args)
  
 Jobs to be done:
   1.Create a class and declaring main.
   2.Inside the main creating a empty linked list and adding elements in hash set using add() method.
   3.Display the linked list element.
   4.Display all the set elements using Iterator interface   
 */


package com.java.core.mapsets;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class TraversalLinkedList {

    public static void main(String[] args) {

         LinkedHashSet hash = new LinkedHashSet();

         hash.add("Java");
         hash.add("Python");
         hash.add("C++");
         hash.add("Ruby");
         hash.add("Javascript");

         Iterator setIterator = hash.iterator();

         while(setIterator.hasNext()){
               System.out.println(setIterator.next());
         }

    }

}