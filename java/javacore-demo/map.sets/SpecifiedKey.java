/*
 1)Write a Java program to copy all of the mappings from the specified map to another map?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
2.Entities
   - SpecifiedKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as SpecifiedKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called bikes.
   3.Adding the values using put() method and creating another hashMap bikes2 for copying from bikes to bikes2 using putAll() method.
   4.Printing the copied hashMap bikes2.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/

package com.java.core.mapsets;

import java.util.HashMap;

public class SpecifiedKey {

   public static void main(String args[]) {
	   
	  //Creating hashMap 
      HashMap<Integer, String> bikes = new HashMap<Integer, String>();

      /*Adding elements to HashMap*/
      bikes.put(4, "Bajaj");
      bikes.put(2, "TVS Motor Company");
      bikes.put(3, "Hero Motocorp");
      bikes.put(5, "Suzuki ");
      bikes.put(1, "Royal Enfield");
      
      HashMap<Integer, String> bikes2 = new HashMap<Integer, String>();
      
      
      //Copy all of the mappings from the specified map to another map
      bikes2.putAll(bikes);
      System.out.println(bikes2);
   }
}