/*
 2) Write a Java program to test if a map contains a mapping for the specified key?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
   - Count the size of mappings in a map
2.Entities
   - ContainsKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as ContainsKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called bikes.
   3.Adding the values using put() method and using containsKey() method find the specified key value map is containing or not.
   4.Count the size of mappings in a map using size() method.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/

package com.java.core.mapsets;

import java.util.HashMap;

public class ContainsKey {
	 public static void main(String args[]) {
		   
		  //Creating hashMap 
	      HashMap<Integer, String> bikes = new HashMap<Integer, String>();

	      /*Adding elements to HashMap*/
	      bikes.put(24, "Bajaj");
	      bikes.put(20, "TVS Motor Company");
	      bikes.put(13, "Hero Motocorp");
	      bikes.put(5, "Suzuki ");
	      bikes.put(1, "Royal Enfield");
	      
	      //Testing if a map contains a mapping for the specified key
	      System.out.println("---------Checking '20' key value map containing or not----------");
	      System.out.println(bikes.containsKey(20));
	      
	      System.out.println("---------Checking '8' key value map containing or not----------");
	      System.out.println(bikes.containsKey(8));
	      
	      //Count the size of mappings in a map
	      System.out.println("---------Count the size of mappings in a map----------");
	      System.out.println(bikes.size());
	      
	   }
}
