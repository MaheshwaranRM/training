/*
4) Write a Java program to get the portion of a map whose keys range from a given key to another key?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called bikes.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/

package com.java.core.mapsets;

import java.util.TreeMap; 

public class GetPortion {

	public static void main(String[] args) {
		
		 //Creating TreeMap and SortedMap
	      TreeMap<Integer, String> bikes = new TreeMap<Integer, String>();
	      
	      /*Adding elements to HashMap*/
	      bikes.put(34, "Bajaj");
	      bikes.put(20, "TVS");
	      bikes.put(13, "Hero");
	      bikes.put(5, "Suzuki ");
	      bikes.put(1, "Royal Enfield");
	     
		  System.out.println("-------------Sub map key-values------------------- \n" + bikes.subMap(20, 40));
	}

}
