/*
  Entity
 	ErrorCode
  
  Method Signature
	  1.ErrorCodes(int code, String message);
	  2.public int getCode();
	  3.public String getMessage();
  
  Jobs to be Done
	  1.Create variable called code of type int
	  2.Create a variable called message of type String
	  3.Create a Constructor with code and message as parameter
	  4.Create another method called getCode of return type int that returns code
	  5.Create another method called getMessage of return type String that returns message
	  6.Create Enum type ErrorCodes with Code and Message
  
  Pseudo Code
  
	  public enum ErrorCode {
	
		CONNECTION_FAILD("Connection Faild"),
		POSTAL_CODE_EMPTY("Postal Code should not Empty"),
		ADDRESS_CREATION_FAILD("Address Creation Faild"),
		ADDRESS_ID_EMPTY("The Id Field cannot be Empty"),
		ADDRESS_UPDATE_FAILD("Address Updation Faild"),
		PERSON_CREATION_FAILD("Person Creation Faild"),
		PERSON_NOT_EXIST("Person not Exist"),
		PERSON_ID_EMPTY("Person id cannot be Empty"),
		PERSON_DELETE_FAILD("Person Delete Faild"),
		ADDRESS_DELETE_FAILD("Address Delete Faild"),
		ADDRESS_ID_DELETE("ID not found"),
		PS_ERROR("PrepareStatement Error in StatementSetter Method"),
		RESULT_ERROR("Result Error in resultSetter Method"),
		PERSON_UPDATION_FAILD("Person Updation Faild"),
		PERSON_UNIQE("Person Uniqe check Faild"),
		PERSON_NAME_CHECK("Error occured in personNameCheck method"),
		ADDRESS_NOT_FOUND("Address Not Found"),
		EMAIL_ID_EXIST("Email already Exist"),
		EMAIL_UNIQUE("Error occured in emailUnique method"),
		PERSON_EXSITS("Person already Exist"),
		CHECK_ALL_FIELDS("Enter All the field Correctly"),
		ERROR_CONNECTION("Error while Closing Connection"),
		NAME_EXIST("Person with the Same Name already Exist"),
		DATE_FORMATE("Date formate should be (dd-mm-yyyy)"),
		POSTAL_CODE("Postal code should not be Empty"),
		ADDRESS_EXIST("Address Already Exist"),
		ERROR_ON_COMIT("Unable to Comit or Rollback"),
		PERSON_READ_FAILD("Unable to Read Person"),
		ADDRESS_READ_FAILD("Unable to Read Address");
		
		public String message;
		
		ErrorCode(String message) {
			this.message = message;
		}
	
		public String getMessage() {
			return message;
		}
	
	}

	 	
	 	

*/

package com.kpr.training.jdbc.exception;

public enum ErrorCode {
	
	CONNECTION_FAILD("Connection Faild"),
	POSTAL_CODE_EMPTY("Postal Code should not Empty"),
	ADDRESS_CREATION_FAILD("Address Creation Faild"),
	ADDRESS_ID_EMPTY("The Id Field cannot be Empty"),
	ADDRESS_UPDATE_FAILD("Address Updation Faild"),
	PERSON_CREATION_FAILD("Person Creation Faild"),
	PERSON_NOT_EXIST("Person not Exist"),
	PERSON_ID_EMPTY("Person id cannot be Empty"),
	PERSON_DELETE_FAILD("Person Delete Faild"),
	ADDRESS_DELETE_FAILD("Address Delete Faild"),
	ADDRESS_ID_DELETE("ID not found"),
	PS_ERROR("PrepareStatement Error in StatementSetter Method"),
	RESULT_ERROR("Result Error in resultSetter Method"),
	PERSON_UPDATION_FAILD("Person Updation Faild"),
	PERSON_UNIQE("Person Uniqe check Faild"),
	PERSON_NAME_CHECK("Error occured in personNameCheck method"),
	ADDRESS_NOT_FOUND("Address Not Found"),
	EMAIL_ID_EXIST("Email already Exist"),
	EMAIL_UNIQUE("Error occured in emailUnique method"),
	PERSON_EXSITS("Person already Exist"),
	CHECK_ALL_FIELDS("Enter All the field Correctly"),
	ERROR_CONNECTION("Error while Closing Connection"),
	NAME_EXIST("Person with the Same Name already Exist"),
	DATE_FORMATE("Date formate should be (dd-mm-yyyy)"),
	POSTAL_CODE("Postal code should not be Empty"),
	ADDRESS_EXIST("Address Already Exist"),
	ERROR_ON_COMIT("Unable to Comit or Rollback"),
	PERSON_READ_FAILD("Unable to Read Person"),
	ADDRESS_READ_FAILD("Unable to Read Address");
	
	public String message;
	
	ErrorCode(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
