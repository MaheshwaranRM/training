package com.kpr.training.jdbc.testing;
/*
Problem Statement
1.To Test All possible conditions for Person Service 

Requirement
1.To Test All possible conditions for Person Service

Entity
1.ServicesTest
2.PersonService
3.AppException
4.Connection
5.Person
6.Address

Method Signature
1.public void setUp()
2.public void insertPersonWithAddressId() 
3.public void insertPersonWithAddress()
4.public void readPersonWithAddress()
5.public void readPersonWithOutAddress()
6.public void insertPerson();
7.public void readPerson();
8.public void readAllPerson();
9.public void updatePerson()
10.public void deletePerson()

Jobs to be done:
->public void setUp()
 1.Create Person and Address class with values.
 2.Prepare personService object of type PersonService.

 @Test insertPersonWithAddress(Person Insertion with Already Created Address Id) 
 1.Invoke the PersonServices class insertPeronAddress method to insert
Person and Address values.
 2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).
 
@Test insertPersonWithAddress(Person Insertion with Address) 
 1.Invoke the PersonServices class insertPeronAddress method to insert
Person and Address values.
 2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).

@Test for readPerson
1.Invoke personService readPerson method (pass personId and boolean value to add address 'true")
and store in List<Person> objectList.
   
@Test for readPerson
2.Invoke personService readPerson method (pass personId and boolean value for without address 'false")
and store in List<Person> objectList.
   
@Test for insertPerson
1.Invoke personService insertPerson method (pass person object) 
and store in insertPersonStatus.
2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).

@Test for readPerson
1.Invoke personService readPerson method (pass person id) and
store in List<Person> objectList .
2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).

@Test for readPerson
1.Invoke personService readAllPerson method and
store in List<Person> objectList.
2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).

@Test for updatePerson
1.Invoke personService updatePerson method (pass person object) 
and store in udpateStatus.
2.AssertJUnit assertTrue method (pass Check udpateStatus is not equal to false ).

@Test for deletePerson
1.Invoke personService deletePerson method (pass person id) 
and store in deleteStatus.
2.AssertJUnit assertTrue method (pass Check deleteStatus is not equal to false ).

Psudeo Code:
public class PersonSerivceTest {
	long id;
	long personWithAddressId;
	long personWithOutAddressId;
	long personUpdateId = 1;
	PersonService personService;
	Person person1;
	Person person2;
	Person person3;
	Address address1;
	Address address2;
	Person personUpdate;
	Person personGetUpdated;
	Person personInsert;
	Person personCreateWithAddressId;
	Person personAddressCreate;
	Connection connection;
	ConnectionService connectionService;

	@BeforeClass
	public void setUp() throws Exception {
		personService = new PersonService();
		connectionService = new ConnectionService();
		connection = connectionService.initConnection();
		personCreateWithAddressId = new Person("Varun", "Varun@gmail.com", 0, Date.valueOf("2001-01-18"));
		personAddressCreate = new Person("Praveen", "Praveen@gmail.com", 0, Date.valueOf("2000-09-20"));
		person1 = new Person("Karun", "Karun@gmail.com", 2, Date.valueOf("2000-11-18"));
		personInsert = new Person("Kumar", "Kumar@gmail.com", 2, Date.valueOf("2000-09-16"));
		personUpdate = new Person(personUpdateId, "Sajith", "Sajith@gmail.com", 2, Date.valueOf("2000-01-11"));
		address1 = new Address("Perumal", "Kanchipuram", 601200);
		address2 = new Address("RajivGandhi", "Tirupur", 641602);
	}

	@Test(priority = 1, description = "Person Insertion with Address Id")
	public void insertPersonWithAddressId() throws AppException, SQLException {
		personWithAddressId = personService.insertPeronAddress(personCreateWithAddressId, address1, connection);
		Assert.assertTrue(personWithAddressId > 0);
	}

	@Test(priority = 2, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws AppException, SQLException {
		personWithOutAddressId = personService.insertPeronAddress(personAddressCreate, address2, connection);
		Assert.assertTrue(personWithOutAddressId > 0);
	}

	@Test(priority = 3, description = "Try Creating person with duplicate email", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Duplicate email address found")
	public void insertPersonSecondTest() throws SQLException {
		personService.insertPeronAddress(personCreateWithAddressId, address1, connection);

	}

	@Test(priority = 4, description = "Person Insertion with Address")
	public void readPersonWithAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readPerson(personWithAddressId, true, connection).toString() != null);
	}

	@Test(priority = 5, description = "Person Insertion with out Address")
	public void readPersonWithOutAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readPerson(personWithOutAddressId, false, connection) != null);
	}

	@Test(priority = 6, description = "Person Creation with unique email")
	public void insertPerson() throws AppException, SQLException {
		this.id = personService.createPerson(person1, connection);
		Assert.assertTrue(this.id > 0);
	}

	@Test(priority = 7, description = "Read Only One Person")
	public void readPerson() throws AppException, SQLException {
		System.out.println(this.id);
		person2 = personService.readPerson(this.id, connection);
		person1.id = this.id;
		Assert.assertEquals(person2.toString(), person1.toString());
	}

	@Test(priority = 8, description = "Read All Person With Address")
	public void readAllPersonWithAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readAllPerson(connection).toString() != null);
	}

	@Test(priority = 9, description = "Update Only One Person")
	public void updatePerson() throws AppException, SQLException {
		personService.updatePerson(personUpdate, personUpdateId, connection);
		personGetUpdated = personService.readPerson(this.personUpdateId, connection);
		Assert.assertEquals(personGetUpdated.toString(), personUpdate.toString());
	}

	@Test(priority = 10, description = "Delete Only One Person")
	public void deletePerson() throws AppException, SQLException {
		personService.deletePerson(this.id, connection);
	}

	@AfterClass
	public void connectionClose() {
		try {
			connectionService.releaseConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}


@author SA
*
*/

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.kpr.training.jdbc.connections.ConnectionPool;
import com.kpr.training.jdbc.connections.ConnectionService;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.services.AddressService;
import com.kpr.training.jdbc.services.PersonService;

public class PersonSerivceTest {
	
	public ConnectionPool thread1 = new ConnectionPool();
	public ConnectionPool thread2 = new ConnectionPool();
	public ConnectionPool thread3 = new ConnectionPool();
	
	long id;
	long personWithAddressId;
	long personWithOutAddressId;
	long personUpdateId = 0;
	long addressId;
	PersonService personService;
	Person person1;
	Person person2;
	Person person3;
	Address address1;
	Address address2;
	Person personUpdate;
	Person personGetUpdated;
	Person personInsert;
	Person personCreateWithAddressId;
	Person personAddressCreate;
	
	ConnectionService connectionService;
	AddressService addressService;
	ArrayList<Object> checkReadPersonAddress;
	ArrayList<Object> checkReadPerson;

	@BeforeClass
	
	public void setUp() throws Exception {
		Date date =  Date.valueOf("2001-03-17");
		personService = new PersonService();
		addressService = new AddressService();
		personCreateWithAddressId = new Person("Varun", "Kumaran", "Varun@gmail.com", 0, date);
		personAddressCreate = new Person("Praveen", "Kumaran","Praveen@gmail.com", 0, personService.convertDate("28-01-2000"));
		person1 = new Person("Karun", "Raj","Karun@gmail.com", 2, personService.convertDate("20-09-2002"));
		personInsert = new Person("Kumar", "Vel", "Kumar@gmail.com", 2, personService.convertDate("18-09-2000"));
		personUpdate = new Person(personUpdateId ,"Rajith", "Raja","Rajith@gmail.com", 1, personService.convertDate("10-10-2001"));
		address1 = new Address("Perumal", "Kanchipuram", 601200);
		address2 = new Address("OMR", "Chennai", 600028);
	    checkReadPersonAddress = new ArrayList<Object>();
	    checkReadPerson = new ArrayList<Object>();
	}

	@Test(priority = 1, description = "Person Insertion with Address Id")
	public void insertPersonWithAddressId() throws Exception {
		
		Connection connection = ConnectionService.initConnection();
		personWithAddressId = personService.createPersonAddress(personCreateWithAddressId, address1, thread1.get());
		personCreateWithAddressId.id = personWithAddressId;
		addressId = personService.readPerson(personWithAddressId, connection).getAddressId();
		personCreateWithAddressId.addressId = addressId;
		address1.id = addressId;
		checkReadPersonAddress.add(personCreateWithAddressId);
		checkReadPersonAddress.add(address1);
		Assert.assertTrue(personWithAddressId > 0);
	}
	@Test(priority = 2, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws Exception {
		
		Connection connection = thread1.get();
		personWithOutAddressId = personService.createPersonAddress(personAddressCreate, address2, thread1.get());
		personAddressCreate.id = personWithOutAddressId;
		addressId = personService.readPerson(personWithOutAddressId, connection).getAddressId();
		personAddressCreate.addressId = addressId;
		checkReadPerson.add(personAddressCreate);
		Assert.assertTrue(personWithOutAddressId > 0);
	}

	@Test(priority = 3, description = "Try Creating person with duplicate email", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Duplicate email address found")
	public void insertPersonWithDuplicateEmail() throws Exception {
		Connection connection = thread1.get();
		//personService.createPersonAddress(personCreateWithAddressId, address1, connection);

	}
	@Test(priority = 4, description = "Read Person with Address")
	public void readPersonWithAddress() throws Exception {
		Connection connection = thread1.get();
		//Assert.assertNotNull(personService.read(personWithAddressId, true, connection));
	}

	@Test(priority = 5, description = "Read Person with out Address")
	public void readPersonWithOutAddress() throws Exception {
		
		Connection connection = thread1.get();
		//Assert.assertNotNull(personService.read(personWithOutAddressId, false, connection));
	}

	@Test(priority = 6, description = "Delete Person With Address")
	public void deletePersonAddress() throws Exception {
		personService.delete(personWithAddressId);
	}
	
	@Test(priority = 7, description = "Person Creation with unique email")
	public void insertPerson() throws Exception {
		this.id = personService.create(person1);
		Assert.assertTrue(this.id > 0);
	}

	@Test(priority = 8, description = "Read Only One Person")
	public void readPerson() throws Exception {
		System.out.println(this.id);
		person2 = personService.read(this.id);
		person1.id = this.id;
		System.out.println(person2.toString());
		System.out.println(person1.toString());
		Assert.assertEquals(person2.toString(), person1.toString());
	}

	@Test(priority = 9, description = "Read All Person With Address")
	public void readAllPersonWithAddress() throws Exception {
		Assert.assertTrue(personService.readAll().toString() != null);
	}

	@Test(priority = 10, description = "Update Only One Person")
	public void updatePerson() throws Exception{
		personService.update(personUpdate, personUpdateId);
		personGetUpdated = personService.read(this.personUpdateId);
		Assert.assertEquals(personGetUpdated.toString(), personUpdate.toString());
	}

	@Test(priority = 11, description = "Delete Only One Person")
	public void deletePerson() throws Exception {
		personService.delete(this.id);
	}
	
	/*@Test(priority = 12, description = "Create Persons Using CSV file ")
	public void InsertPersonUsingCsvFile() throws Exception {
		String csvFilePath = "C:/Users/santh/eclipse-workspace/jdbc-services/resources/PersonInsertion.csv";
		personService.CsvPersonInsertion(csvFilePath ,thread1.get());
	}*/
	
	
	@AfterClass
	public void connectionClose() {
		try {
			ConnectionService.closeConnection(thread1.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
		