//package students;
import programs.*;

public class Student {
	
	public String name;
	public int rollNo;
	public int year;
	
	//Constructor with three parameters
	public Student(String name,int rollNo,int year) {
		this.name = name;
		this.rollNo = rollNo;
		this.year = year;
	}
	
	//private constructor with two parameters
	private Student(String name,int year) {
		System.out.println(name + year);
	}
	
	//Constructor with no parameters i.e. default constructor
	public Student() {
		System.out.println(name );
		
	}
	
	//Object initializer
	{
		rollNo = 1;
		
	}
	
	//Method definition
	public void printDetails() {
		System.out.println("Student name :" + name + " \n " + "Roll no. :" + rollNo + "\n" + "Year:" + year);
	}

	//Main function
public static void main(String args[]){
	
Student stud = new Student("Praveen",12,3);
stud.printDetails();
Student stud1 = new Student("Mahesh",13);
stud1.printDetails();
Student stud2 = new Student();
Student stud3 = new Student("Rahul",21);
School school = new School();
school.run1();                   //compile time error : private access in School
Teacher teacher = new Teacher();
teacher.run();                   //compile time error : protected access in Teacher
stud.run(); 
}
}
