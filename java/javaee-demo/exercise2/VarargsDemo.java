/* 
    Question : 
	
	Demonstrate overloading with varArgs
*/

/*  
    Requirements : To demonstrate overloading with varArgs.
*/

/*  
    Entities : public class VarargsDemo
*/

/*  
    Method Signature : public void Varargs(int... args)
	                   public void Varargs(char... args)
					   public void Varargs(double... args)
					   public void main(String args[])
*/

/*  Jobs to be done : 1. A method Varargs is created with one integer parameter.
                      2. Inside this method length of the arguments and values of arguments is printed.
					  3. A method Varargs is created with one char parameter.
					  4. Inside this method length of the arguments and values of arguments is printed.
					  5. A method Varargs is created with one double parameter.
					  6. Inside this method length of the arguments and values of arguments is printed.
					  7. Inside the main function call all the methods with suitable arguments.
*/
                       
public class VarargsDemo {
	
   public void Varargs(int... args) {
      System.out.println("\nNumber of int arguments are: " + args.length);
      System.out.println("The int argument values are: ");
      for (int i : args)
      System.out.println(i);
   }
   public void Varargs(char... args) {
      System.out.println("\nNumber of char arguments are: " + args.length);
      System.out.println("The char argument values are: ");
      for (char i : args)
      System.out.println(i);
   }
   public void Varargs(double... args) {
      System.out.println("\nNumber of double arguments are: " + args.length);
      System.out.println("The double argument values are: ");
      for (double i : args)
      System.out.println(i);
   }
   public static void main(String args[]) {
      Varargs(4, 9, 1, 6, 3);
      Varargs('A', 'B', 'C');
      Varargs(5.9, 2.5);
   }
}