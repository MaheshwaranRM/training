/*
    Question :

    Write a class that implements the CharSequence interface found in the java.lang package.
    Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.
*/

/*
    Requirements : To write a class that implements CharSequence interface.
                   This implementation should return the string backwards.
				   To write a main method to test the class.
				   To call all four methods.
*/

/*  
    Entities : public class CharSequenceDemo
	           public CharSequenceDemo
*/

/*
    Method Signature : private int fromEnd(int i)
                       public char charAt(int i) 
					   public int length()
					   public CharSequence subSequence(int start, int end)
					   public String toString()
					   private static int random(int max)
					   public static void main(String[] args) 
*/

/*  
    Jobs to be done : 1. A class CharSequenceDemo is created which implements CharSequence.
	                  2. A variable s is declared with datatype String.
					  3. A constructor CharSequence is created with one parameter.
					  4. The parameter passed to this constructor is assigned to a variable s using "this" operator.
                      5. A method fromEnd() is defined with one parameter which returns s.length() - 1 - i.
					  6. A method charAt() is defined with one parameter.
					  7. Inside the charAt() method an if statement is created with i < 0 || i >= s.length().
					  8. If the condition is true it throws an exception StringIndexOutOfBoundsException(i).
					  9. A method length() is created which returns length of s.
					  10. A method subSequence is defined which is of type CharSequence with two parameters.
					  11. Three if conditions are given which are (start < 0),(end > s.length()),(start > end) which throws suitable exceptions.
					  12. Return sub.reverse().
					  13. A method toString() is defined.
					  14. Inside the method toString() a object s is created for class StringBuilder.
					  15. s.reverse().toString() function is reversed.
					  16. Inside main function a object s is created for class CharSequenceDemo.
					  17. start and end is intialized with random function.
					  18. The variable s is displayed.
*/
				   

public class CharSequenceDemo implements CharSequence {
    private String s;

    public CharSequenceDemo(String s) {
        this.s = s;
    }

    private int fromEnd(int i) {
        return s.length() - 1 - i;
    }

    public char charAt(int i) {
        if ((i < 0) || (i >= s.length())) {
            throw new StringIndexOutOfBoundsException(i);
        }
        return s.charAt(fromEnd(i));
    }

    public int length() {
        return s.length();
    }

    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }
        if (end > s.length()) {
            throw new StringIndexOutOfBoundsException(end);
        }
        if (start > end) {
            throw new StringIndexOutOfBoundsException(start - end);
        }
        StringBuilder sub = 
            new StringBuilder(s.subSequence(fromEnd(end), fromEnd(start)));
        return sub.reverse();
    }

    public String toString() {
        StringBuilder s = new StringBuilder(this.s);
        return s.reverse().toString();
    }
	
    private static int random(int max) {
        return (int) Math.round(Math.random() * (max+1));
    }

    public static void main(String[] args) {
        CharSequenceDemo s =
            new CharSequenceDemo("KPR Institute of Engineering and Technology");

        for (int i = 0; i < s.length(); i++) {
            System.out.print(s.charAt(i));
        }
        
        System.out.println("");

        int start = random(s.length() - 1);
        int end = random(s.length() - 1 - start) + start;
        System.out.println(s.subSequence(start, end));

        System.out.println(s);

    }
}